package com.workerman.server.service;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.workerman.server.common.Const;
import com.workerman.server.common.ErrorCode;
import com.workerman.server.config.ApplicationConfig;
import com.workerman.server.mapper.OrderMapper;
import com.workerman.server.utils.FileUtil;
import com.workerman.server.vo.OrderAddRequestVo;
import com.workerman.server.vo.OrderListRequestVo;
import com.workerman.server.vo.OrderListResponseResultVo;
import com.workerman.server.vo.OrderModifyRequestVo;
import com.workerman.server.vo.OrderResponseVo;
import com.workerman.server.vo.ResultVo;

import lombok.extern.slf4j.Slf4j;



@Slf4j
@Service
public class OrderService {
	
	@Autowired
	private OrderMapper orderMapper;
	
	@Autowired
    private ApplicationConfig applicationConfig;
	
	/**
	 * 주문목록조회
	 * @param orderListRequest
	 * @return
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public OrderListResponseResultVo selectOrderList(OrderListRequestVo orderListRequest){
		
		OrderListResponseResultVo orderListResponseResultVo = new OrderListResponseResultVo();
		orderListResponseResultVo.setNow_page(orderListRequest.getNow_page());
		orderListResponseResultVo.setPage_size(orderListRequest.getPage_size());
		orderListResponseResultVo.setTotal_record(orderMapper.selectOrderListCnt(orderListRequest));
		orderListResponseResultVo.setList(orderMapper.selectOrderList(orderListRequest));
		
		return orderListResponseResultVo;
		
	}
	
	/**
	 * 주문등록
	 * @param orderAddVo
	 * @param content_img
	 * @return
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public ResultVo insertOrder(OrderAddRequestVo orderAddVo, MultipartFile fileContent) throws IOException{
		
		if(fileContent != null && 0 < fileContent.getSize()) {
			
			String prefix_upload_path = applicationConfig.getFileRootPath()+applicationConfig.getFileUploadPath();
			
			String real_file_name = System.currentTimeMillis()+fileContent.getOriginalFilename();
			
			String attachPath = prefix_upload_path + "/"
					+ real_file_name.substring(0, 4) + "/"
					+ real_file_name.substring(4, 6) + "/"
					+ real_file_name.substring(6, 8) + "/";
			
			String dbPath = applicationConfig.getFileUploadPath() + "/"
					+ real_file_name.substring(0, 4) + "/"
					+ real_file_name.substring(4, 6) + "/"
					+ real_file_name.substring(6, 8) + "/";
			
			log.debug("@ attachPath : " + attachPath);
			log.debug("@ dbPath : " + dbPath);
			log.debug("@ real_file_name : " + real_file_name);
			
			File file = new File(attachPath);
			file.mkdirs();
			FileUtil.saveFile(fileContent, attachPath, real_file_name);
			orderAddVo.setContent_img(dbPath+real_file_name);
		}
		
		orderAddVo.setState_idx(Const.ORDER_STATE_IDX_ORDER); // 주문신청
		orderMapper.insertOrder(orderAddVo);
		
		return new ResultVo();
	}
	
	/**
	 * 주문상세정보조회
	 * @param o_idx
	 * @return
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public OrderResponseVo selectOrderByIdx(int o_idx) {
		
		OrderResponseVo orderResponseVo = orderMapper.selectOrderByIdx(o_idx);
		if(orderResponseVo == null) {
			orderResponseVo = new OrderResponseVo();
			orderResponseVo.setReturnCd(ErrorCode.EMPTY_DATA.getCode());
			orderResponseVo.setReturnMsg(ErrorCode.EMPTY_DATA.getMessage());
		}
		return orderResponseVo;
	}
	
	/**
	 * 주문갱신처리
	 * @param orderModifyRequestVo
	 * @param fileContent
	 * @return
	 * @throws IOException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public ResultVo updateOrder(OrderModifyRequestVo orderModifyRequestVo, MultipartFile fileContent) throws IOException{
		
		ResultVo resultVo = new ResultVo();
		
		OrderResponseVo orderResponseVo = orderMapper.selectOrderByIdx(orderModifyRequestVo.getO_idx());
		
		if(orderResponseVo == null) {
			resultVo.setReturnCd(ErrorCode.EMPTY_DATA.getCode());
			resultVo.setReturnMsg(ErrorCode.EMPTY_DATA.getMessage());
			return resultVo;
		}
		
		if(fileContent != null && 0 < fileContent.getSize()) {
			
			String prefix_upload_path = applicationConfig.getFileRootPath()+applicationConfig.getFileUploadPath();
			
			String real_file_name = System.currentTimeMillis()+fileContent.getOriginalFilename();
			
			String attachPath = prefix_upload_path + "/"
					+ real_file_name.substring(0, 4) + "/"
					+ real_file_name.substring(4, 6) + "/"
					+ real_file_name.substring(6, 8) + "/";
			
			String dbPath = applicationConfig.getFileUploadPath() + "/"
					+ real_file_name.substring(0, 4) + "/"
					+ real_file_name.substring(4, 6) + "/"
					+ real_file_name.substring(6, 8) + "/";
			
			log.debug("@ attachPath : " + attachPath);
			log.debug("@ dbPath : " + dbPath);
			log.debug("@ real_file_name : " + real_file_name);
			
			File file = new File(attachPath);
			file.mkdirs();
			FileUtil.saveFile(fileContent, attachPath, real_file_name);
			orderModifyRequestVo.setContent_img(dbPath+real_file_name);
		}
		
		if(orderResponseVo.getContent_img() != null) { // 이전파일삭제
			FileUtil.removeFile(applicationConfig.getFileRootPath()+orderResponseVo.getContent_img());
		}
		
		orderMapper.updateOrderByIdx(orderModifyRequestVo);
		
		return new ResultVo();
	}
	
	/**
	 * 주문삭제
	 * @param o_idx
	 * @return
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public ResultVo deleteOrder(int o_idx) {
		
		ResultVo resultVo = new ResultVo();
		
		OrderResponseVo orderResponseVo = orderMapper.selectOrderByIdx(o_idx);
		if(orderResponseVo == null) {
			resultVo.setReturnCd(ErrorCode.EMPTY_DATA.getCode());
			resultVo.setReturnMsg(ErrorCode.EMPTY_DATA.getMessage());
			return resultVo;
		}
		
		if(orderResponseVo.getContent_img() != null) { // 이전파일삭제
			FileUtil.removeFile(applicationConfig.getFileRootPath()+orderResponseVo.getContent_img());
		}
		
		orderMapper.deleteOrderByIdx(o_idx);
		
		return new ResultVo();
	}
	
}
