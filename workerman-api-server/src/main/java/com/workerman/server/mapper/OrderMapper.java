package com.workerman.server.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.workerman.server.vo.OrderAddRequestVo;
import com.workerman.server.vo.OrderListRequestVo;
import com.workerman.server.vo.OrderListResponseVo;
import com.workerman.server.vo.OrderModifyRequestVo;
import com.workerman.server.vo.OrderResponseVo;

@Mapper
public interface OrderMapper {

	/**
	 * 주문목록조회
	 * @param orderListRequest
	 * @return
	 */
	public List<OrderListResponseVo> selectOrderList(OrderListRequestVo orderListRequest);
	
	/**
	 * 주문목록수
	 * @param orderListRequest
	 * @return
	 */
	public int selectOrderListCnt(OrderListRequestVo orderListRequest);
	
	/**
	 * 주문등록
	 * @param orderAddVo
	 * @return
	 */
	public int insertOrder(OrderAddRequestVo orderAddRequestVo);
	
	/**
	 * 주문상세조회
	 * @param o_idx
	 * @return
	 */
	public OrderResponseVo selectOrderByIdx(int o_idx);
	
	/**
	 * 주문갱신
	 * @param orderModifyRequestVo
	 * @return
	 */
	public int updateOrderByIdx(OrderModifyRequestVo orderModifyRequestVo);

	/**
	 * 주문삭제
	 * @param o_idx
	 * @return
	 */
	public int deleteOrderByIdx(int o_idx);
}
