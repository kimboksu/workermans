package com.workerman.server.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix="workman")
public class ApplicationConfig {

    private String fileRootPath;
    private String fileUploadPath;

}
