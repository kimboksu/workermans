package com.workerman.server.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.workerman.server.service.OrderService;
import com.workerman.server.vo.OrderAddRequestVo;
import com.workerman.server.vo.OrderListRequestVo;
import com.workerman.server.vo.OrderListResponseResultVo;
import com.workerman.server.vo.OrderModifyRequestVo;
import com.workerman.server.vo.OrderResponseVo;
import com.workerman.server.vo.ResultVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@RestController
@RequestMapping(path = "/api/v1/order")
@Api(description = "주문관련정보조회")
public class OrderController extends BaseController{

	@Autowired
	private OrderService orderService;

	@ApiOperation(value = "주문>주문목록조회")
    @PostMapping(path="/list", produces = { "application/json" }, consumes = { "application/json" })
	public ResponseEntity<OrderListResponseResultVo> list(@ApiParam(value = "주문목록조회" ) @RequestBody OrderListRequestVo orderListRequestVo) throws Exception {
		return new ResponseEntity<OrderListResponseResultVo>(orderService.selectOrderList(orderListRequestVo), HttpStatus.OK);
	}
	
	@ApiOperation(value = "주문>주문등록처리")
    @PostMapping(path="/add")
	public ResponseEntity<ResultVo> add(
			@ApiParam(value = "주문등록처리" ) @Valid @ModelAttribute OrderAddRequestVo orderAddVo, 
			@ApiParam(value = "첨부이미지파일") @RequestPart(name="fileContent", required = false) MultipartFile fileContent) throws Exception {
		return new ResponseEntity<ResultVo>(orderService.insertOrder(orderAddVo, fileContent), HttpStatus.OK);
	}
	

	@ApiOperation(value = "주문>주문상세정보조회")
    @GetMapping(path="/detail/{o_idx}")
	public ResponseEntity<OrderResponseVo> detail(@ApiParam(value = "주문상세정보조회" ) @PathVariable("o_idx") int o_idx) throws Exception {
		return new ResponseEntity<OrderResponseVo>(orderService.selectOrderByIdx(o_idx), HttpStatus.OK);
	}
	
	@ApiOperation(value = "주문>주문갱신처리")
    @PutMapping(path="/modify")
	public ResponseEntity<ResultVo> modify(
			@ApiParam(value = "주문갱신처리" ) @Valid @ModelAttribute OrderModifyRequestVo orderModifyRequestVo,
			@ApiParam(value = "첨부이미지파일") @RequestPart(name="fileContent", required = false) MultipartFile fileContent) throws Exception {
		return new ResponseEntity<ResultVo>(orderService.updateOrder(orderModifyRequestVo, fileContent), HttpStatus.OK);
	}
	
	@ApiOperation(value = "주문>주문삭제처리")
    @DeleteMapping(path="/delete/{o_idx}")
	public ResponseEntity<ResultVo> delete(@ApiParam(value = "주문삭제처리" ) @PathVariable("o_idx") int o_idx) throws Exception {
		return new ResponseEntity<ResultVo>(orderService.deleteOrder(o_idx), HttpStatus.OK);
	}

}
