package com.workerman.server.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.workerman.server.common.ErrorCode;
import com.workerman.server.vo.ResultVo;

public class BaseController {

	protected final Log log = LogFactory.getLog(getClass());

	/**
	 * 공통리턴메서드
	 * 
	 * @param param
	 * @return
	 */
	public ResponseEntity<?> returnResponse(Object obj) {
		return new ResponseEntity<Object>(obj, HttpStatus.OK);
	}

	/**
	 * 공통리턴메서드[error]
	 * 
	 * @param key
	 * @return
	 */
	public ResponseEntity<?> returnResponse(Exception e) {
		ResultVo result = new ResultVo();
		result.setReturnCd(ErrorCode.FAIL_SYSTEM_ERROR.getCode());
		result.setReturnMsg(ErrorCode.FAIL_SYSTEM_ERROR.getMessage()+" | "+e.getMessage());
		return new ResponseEntity<ResultVo>(result, HttpStatus.OK);
	}
}
