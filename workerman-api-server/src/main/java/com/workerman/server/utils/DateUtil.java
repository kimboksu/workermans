package com.workerman.server.utils;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.i18n.LocaleContextHolder;



public class DateUtil {
	/** log */
	private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);
	
	private static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final SimpleDateFormat formatter = new SimpleDateFormat(DATEFORMAT);
	
	private static final String DATEFORMATMILLI = "yyyy-MM-dd HH:mm:ss.S";
	private static final SimpleDateFormat formattermilli = new SimpleDateFormat(DATEFORMATMILLI);
	
	/**
	 * 시간 String을 지정한 pattern으로 포멧하여 Date형으로 리턴
	 */
	public static Date getDate(String dateString, String pattern) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern, LocaleContextHolder.getLocale());
		return sdf.parse(dateString);		
	}
	
	/**
	 * 오늘 날짜 Date 객체 리턴
	 */
	public static Date getDate() throws Exception {
		return new Date();
	}
	
	/**
	 * 오늘 날짜를 yyyy-MM-dd 형식으로 리턴한다 
	 */
	public static String getDateString() {
		SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd", LocaleContextHolder.getLocale());
		return simpledateformat.format(new Date());
	}
	
	/**
	 * 9999-12-31를 리턴
	 */
	public static String getInfiniteDateString() {
		return "9999-12-31";
	}
	
	/**
	 * 주어진 java.util.Date 객체를 주어진 format 형식으로 리턴한다
	 */
	public static String getDateString(Date date, String pattern) {
		String result = null;
		
		if ( date != null && pattern != null && pattern.length() > 0 ) {
			SimpleDateFormat simpledateformat = new SimpleDateFormat(pattern, LocaleContextHolder.getLocale());
			result = simpledateformat.format(date);
		}
		
		return result;
	}
	
	/**
	 * 현재 달력을 리턴
	 */
	public static Calendar getCalendar() {
		return Calendar.getInstance(LocaleContextHolder.getLocale());
	}
	
	/**
	 * 오늘 날짜를 리턴한다
	 */
	public static int getDay() {
		SimpleTimeZone simpletimezone = new SimpleTimeZone(0x1ee6280, "KST");
		Calendar calendar = Calendar.getInstance(simpletimezone);
		return calendar.get(Calendar.DATE);
	}
	
	/**
	 * 현재 달을 리턴한다
	 */
	public static int getMonth() {
		SimpleTimeZone simpletimezone = new SimpleTimeZone(0x1ee6280, "KST");
		Calendar calendar = Calendar.getInstance(simpletimezone);
		return calendar.get(Calendar.MONTH) + 1;
	}
	
	/**
	 * 현재 년을 리턴한다
	 */
	public static int getYear() {
		SimpleTimeZone simpletimezone = new SimpleTimeZone(0x1ee6280, "KST");
		Calendar calendar = Calendar.getInstance(simpletimezone);
		return calendar.get(Calendar.YEAR);
	}
	
	/**
	 * 달의 마지막 날짜를 리턴
	 */
	public static int getMonthLastDay(int year, int month) {
		switch (month) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12: return (31);
			case 4:
			case 6:
			case 9:
			case 11: return (30);
			default:
				if( ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0) )
					return (29); // 2월 윤년계산을 위해서
				else
					return (28);
		}
	}
	
	public static String getDate(int addDate, String format, String stendadDate) {
		String result = null;
		try {
			
			DecimalFormat df = new DecimalFormat("00");
			
			SimpleDateFormat stendadFomat = new SimpleDateFormat("yyyy-MM-dd");
			
			Calendar currentCalendar = Calendar.getInstance();
			currentCalendar.setTime(stendadFomat.parse(stendadDate));
			
			currentCalendar.add(Calendar.DATE, addDate);
			
			String strYear = Integer.toString(currentCalendar.get(Calendar.YEAR));
			String strMonth = df.format(currentCalendar.get(Calendar.MONTH) + 1);
			String strDay = df.format(currentCalendar.get(Calendar.DATE));
			String strDate = strYear + strMonth + strDay;
			
			SimpleDateFormat defaultFomat = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			Date fDate = defaultFomat.parse(strDate);
			result = sdf.format(fDate);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 날짜단위 연산
	 */
	public static Date addDay(Date date, int day) {
		Calendar cal = Calendar.getInstance();
		
		cal.setTime(date);
		cal.add(Calendar.DATE, day);
		
		return cal.getTime();
	}
	
	/**
	 * 월단위 연산
	 */
	public static Date addMonth(Date date, int month) {
		Calendar cal = Calendar.getInstance();
		
		cal.setTime(date);
		cal.add(Calendar.MONTH, month);
		
		return cal.getTime();
	}
	
	/**
	 * 년단위 연산
	 */
	public static Date addYear(Date date, int year) {
		Calendar cal = Calendar.getInstance();
		
		cal.setTime(date);
		cal.add(Calendar.YEAR, year);
		
		return cal.getTime();
	}
	
	/**
	 * 현재 주의 첫번째 날을 리턴
	 */
	public static Date getFirstDateOfWeek() {
		Date result = null;
		Calendar	cal = Calendar.getInstance(LocaleContextHolder.getLocale());
		cal.add(Calendar.DATE, 1 - cal.get(Calendar.DAY_OF_WEEK));
		result = cal.getTime();
		
		return result; 		
	}
	
	/**
	 * 현재 주의 첫번째 날을 리턴
	 */
	public static String getFirstDateOfWeek(String pattern) {
		Date result = null;
		
		if ( pattern == null || pattern.trim().length() <= 0 ) {
			pattern = "yyyy-MM-dd";
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(pattern, LocaleContextHolder.getLocale());
		
		Calendar	cal = Calendar.getInstance(LocaleContextHolder.getLocale());
		cal.add(Calendar.DATE, 1 - cal.get(Calendar.DAY_OF_WEEK));
		result = cal.getTime();
		
		return sdf.format(result); 		
	}
	
	/**
	 * 현재 월의 첫번째 날을 리턴
	 */
	public static Date getFirstDateOfMonth() {
		Date result = null;
		
		Calendar	cal = Calendar.getInstance(LocaleContextHolder.getLocale());
		cal.add(Calendar.DATE, 1 - Calendar.DATE);
		result = cal.getTime();
		
		return result; 		
	}
	
	/**
	 * 현재 월의 첫번째 날을 리턴
	 */
	public static String getFirstDateOfMonth(String pattern) {
		Date result = null;
		
		if ( pattern == null || pattern.trim().length() <= 0 ) {
			pattern = "yyyy-MM-dd";
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(pattern, LocaleContextHolder.getLocale());
		
		Calendar	cal = Calendar.getInstance(LocaleContextHolder.getLocale());
		cal.add(Calendar.DATE, 1 - Calendar.DATE);
		result = cal.getTime();
		
		return sdf.format(result); 		
	}
	
	/**
	 * 주어진 날짜가 속한 주의 첫번째 날을 리턴
	 */
	public static Date getFirstDateOfWeek(Date date) {
		Date result = null;
		Calendar calendar = Calendar.getInstance(LocaleContextHolder.getLocale());
		
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 1 - calendar.get(Calendar.DAY_OF_WEEK));
		result = calendar.getTime();
		
		return result; 		
	}
	
	/**
	 * 주어진 날짜가 속한 주의 첫번째 날을 리턴
	 */
	public static String getFirstDateOfWeek(Date date, String pattern) {
		Date result = null;
		
		if ( pattern == null || pattern.trim().length() <= 0 ) {
			pattern = "yyyy-MM-dd";
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(pattern, LocaleContextHolder.getLocale());
		
		Calendar calendar = Calendar.getInstance(LocaleContextHolder.getLocale());
		
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 1 - calendar.get(Calendar.DAY_OF_WEEK));
		result = calendar.getTime();
		
		return sdf.format(result); 		
	}
	
	/**
	 * 현재 주의 마지막 날을 리턴
	 */
	public static Date getLastDateOfWeek() {
		Date result = null;
		
		Calendar cal = Calendar.getInstance(LocaleContextHolder.getLocale());
		cal.add(Calendar.DATE, 7 - cal.get(Calendar.DAY_OF_WEEK));
		result = cal.getTime();
		
		return result; 		
	}
	
	/**
	 * 현재 주의 마지막 날을 리턴
	 */
	public static String getLastDateOfWeek(String pattern) {
		Date result = null;
		
		if ( pattern == null || pattern.trim().length() <= 0 ) {
			pattern = "yyyy-MM-dd";
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(pattern, LocaleContextHolder.getLocale());
		
		Calendar cal = Calendar.getInstance(LocaleContextHolder.getLocale());
		cal.add(Calendar.DATE, 7 - cal.get(Calendar.DAY_OF_WEEK));
		result = cal.getTime();
		
		return sdf.format(result); 		
	}
	
	/**
	 * 현재 월의 마지막 날을 리턴
	 */
	public static Date getLastDateOfMonth() {
		Date result = null;
		
		Calendar cal = Calendar.getInstance(LocaleContextHolder.getLocale());
		int day = getMonthLastDay( cal.get(Calendar.YEAR) , cal.get(Calendar.MONTH)+1 );
		cal.set(Calendar.DAY_OF_MONTH, day);
		
		result = cal.getTime();
		
		return result; 		
	}
	
	/**
	 * 현재 월의 마지막 날을 리턴
	 */
	public static String getLastDateOfMonth(String pattern) {
		Date result = null;
		
		if ( pattern == null || pattern.trim().length() <= 0 ) {
			pattern = "yyyy-MM-dd";
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(pattern, LocaleContextHolder.getLocale());
		
		Calendar cal = Calendar.getInstance(LocaleContextHolder.getLocale());
		int day = getMonthLastDay( cal.get(Calendar.YEAR) , cal.get(Calendar.MONTH)+1 );
		cal.set(Calendar.DAY_OF_MONTH, day);
		
		result = cal.getTime();
		
		return sdf.format(result);
	}
	
	/**
	 * 주어진 날짜가 속한 주의 마지막 날을 리턴
	 */
	public static Date getLastDateOfWeek(Date date) {
		Date result = null;
		Calendar calendar = Calendar.getInstance(LocaleContextHolder.getLocale());
		
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 7 - calendar.get(Calendar.DAY_OF_WEEK));
		result = calendar.getTime();
		
		return result; 		
	}
	
	/**
	 * 주어진 날짜가 속한 주의 마지막 날을 리턴
	 */
	public static String getLastDateOfWeek(Date date, String pattern) {
		Date result = null;
		
		if ( pattern == null || pattern.trim().length() <= 0 ) {
			pattern = "yyyy-MM-dd";
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat(pattern, LocaleContextHolder.getLocale());
		
		Calendar calendar = Calendar.getInstance(LocaleContextHolder.getLocale());
		
		calendar.setTime(date);
		calendar.add(Calendar.DATE, 7 - calendar.get(Calendar.DAY_OF_WEEK));
		result = calendar.getTime();
		
		return sdf.format( result ); 		
	}
	
	/**
	 * 주어진 날짜가 속한 주의 마지막 날을 리턴
	 */
	public static int getLastDateOfWeek(int year, int month) {
		Calendar calendar = Calendar.getInstance(LocaleContextHolder.getLocale());
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.DATE, 7 - calendar.get(Calendar.DAY_OF_WEEK));
		
		return calendar.get(Calendar.DAY_OF_WEEK); 		
	}
	
	/**
	 * 주어진 날짜의 속한 달의 마지막 주를 리턴
	 */
	public static int getLastWeekOfMonth(int year, int month) {
		Calendar calendar = Calendar.getInstance(LocaleContextHolder.getLocale());
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.DATE, 7 - calendar.get(Calendar.DAY_OF_WEEK));
		
		return calendar.getActualMaximum(Calendar.WEEK_OF_MONTH); 		
	}
	
	
	/**
	 * Convert date pattern
	 */
	public static String convertDateString( String dateString, String orgPattern, String toPattern) throws Exception {
		
		SimpleDateFormat sdf = new SimpleDateFormat(orgPattern, LocaleContextHolder.getLocale());
		Date tmp = sdf.parse(dateString);
		
		sdf = new SimpleDateFormat(toPattern, LocaleContextHolder.getLocale());
		
		return sdf.format(tmp);
	}
	
	
	/**
	 * 오늘 날짜를 입력받은 패턴에 맞게 포맷한 날짜 문자열을 반환한다.<br>
	 *
	 * @param pattern
	 *            구하고자 하는 날짜 패턴
	 * @return String 입력받은 패턴에 맞게 생성된 날짜 문자열
	 */
	public static String getDate(String pattern) {
		SimpleDateFormat sf = new SimpleDateFormat(pattern);
		return sf.format(new Timestamp(System.currentTimeMillis()));
	}

	/**
	 * 주어진 날짜를 입력받은 패턴에 맞는 날짜 문자열을 반환한다.
	 *
	 * @param date
	 *            Date 객체
	 * @param pattern
	 *            구하고자 하는 날짜 패턴
	 * @return 입력받은 Date 객체로부터 입력받은 패턴에 맞게 생성된 날짜 문자열을 반환한다. 만일 Date 객체가 null
	 *         이거나 pattern이 null 이라면 공백 문자열("")을 반환한다.
	 */
	public static String getDate(Date date, String pattern) {
		if (date == null || pattern == null) {
			return "";
		}
		SimpleDateFormat sf = new SimpleDateFormat(pattern);
		return sf.format(date);
	}

	/**
	 * 주어진 날짜를 입력받은 패턴에 맞는 날짜 문자열을 반환한다.
	 *
	 * @param millis
	 *            시간
	 * @param pattern
	 *            구하고자 하는 날짜 패턴
	 * @return 입력받은 시간으로부터 입력받은 패턴에 맞게 생성된 날짜 문자열을 반환한다. 만일 시간이 0 이거나 pattern이
	 *         null 이라면 공백 문자열("")을 반환한다.
	 */
	public static String getDate(long millis, String pattern) {
		if (millis == 0L || pattern == null) {
			return "";
		}

		SimpleDateFormat sf = new SimpleDateFormat(pattern, LocaleContextHolder.getLocale());
		return sf.format(new Date(millis));
	}

	/**
	 * Gets the datetime. 
	 *
	 * @return "yyyy / MM / dd am[pm] hh:mm"
	 */
	public static String getDatetime() {
		String currDate = getDate("yyyy / MM / dd");
		String currTime = getDate("hh:mm");
		String timePart = getDate("aaa");

		timePart = (timePart.equals("오전")) ? "am" : "pm";

		String datetime = currDate + " " + timePart + " " + currTime;

		return datetime;
	}

	/**
	 * 주어진 달의 마지막 날짜를 반환한다.
	 *
	 * @param year
	 *            년도
	 * @param month
	 *            월 값의 범위는 1~12이다.
	 * @return int 입력받은 달의 마지막 날짜
	 */
	public static int getMaximumDateOfMonth(int year, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month - 1);
		return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 주어진 달의 시작 요일을 반환한다.
	 *
	 * @param year
	 *            년도
	 * @param month
	 *            시작 요일을 구하고자 하는 달로써 값의 범위는 1~12이다.
	 * @return int 입력받은 달의 시작 요일
	 */
	public static int getMinimalDaysInFirstWeek(int year, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month - 1);
		calendar.set(Calendar.DATE, 1);
		return calendar.get(Calendar.DAY_OF_WEEK);
	}

	/**
	 * 주어진 달의 마지막 요일을 반환한다.
	 *
	 * @param year
	 *            년도
	 * @param month
	 *            마지막 요일을 구하고자 하는 달로써 값의 범위는 1~12이다.
	 * @return int 입력받은 달의 마지막 요일
	 */
	public static int getMaximumDaysInLastWeek(int year, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DATE, getMaximumDateOfMonth(year, month));
		return calendar.get(Calendar.DAY_OF_WEEK);
	}

	/**
	 * 주어진 날짜의 요일을 반환한다.
	 *
	 * @param date
	 *            요일을 구하고자 하는 날짜
	 * @return int 요일
	 */
	public static int getDayOfWeek(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_WEEK);
	}

	/**
	 * 현재 시스템 시간을 milliseconds로 반환한다.
	 *
	 * @return long milliseconds
	 */
	public static long getCurrentTimeMillis() {
		try {
			return System.currentTimeMillis();
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * 스트링 문자열을 시간으로.
	 *
	 * @param year String
	 * @param month String
	 * @param day String
	 * @return the date
	 */
	public static Date StringtoDate(String year, String month, String day) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		ParsePosition poss = new ParsePosition(0);
		Date date = format.parse(year + "-" + month + "-" + day, poss);
		return date;
	}

	/**
	 * 스트링 문자열을 시간으로.
	 *
	 * @param currentDate yyyymmdd
	 * @return the date
	 */
	public static Date StringtoDate(String currentDate) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		ParsePosition poss = new ParsePosition(0);
		Date date = format.parse(currentDate, poss);
		return date;
	}

	/**
	 * Stringto date by format.
	 *
	 * @param currentDate String
	 * @param dateFormat String
	 * @return the date
	 */
	public static Date StringtoDateByFormat(String currentDate, String dateFormat) {
		SimpleDateFormat format = new SimpleDateFormat(dateFormat);
		ParsePosition poss = new ParsePosition(0);
		Date date = format.parse(currentDate, poss);
		return date;
	}

	/**
	 * Stringto date time.
	 *
	 * @param time String
	 * @return the date
	 */
	public static Date StringtoDateTime(String time) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;
		try {
			date = format.parse(time);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * Stringto date.
	 *
	 * @param time String
	 * @param form String
	 * @return the date
	 */
	public static Date StringtoDate(String time, String form) {
		SimpleDateFormat format = new SimpleDateFormat(form);
		Date date = null;
		try {
			date = format.parse(time);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 양력을 음력으로 변환한다.<br>
	 * get 메소드를 이용하여 변환된 값을 얻을수 있다.<br>
	 * get("month") - 음력으로 변환된 월<br>
	 * get("day") - 음력으로 변환된 날짜
	 *
	 * @param year
	 *            년
	 * @param month
	 *            월
	 * @param day
	 *            일
	 * @return Hashtable 변환된 음력 날짜를 포함하는 Hashtable
	 */
	public static Hashtable solarToLunar(int year, int month, int day) {
		String[] MonthData = { "1212122322121", "1212121221220", "1121121222120", "2112132122122", "2112112121220", "2121211212120", "2212321121212",
				"2122121121210", "2122121212120", "1232122121212", "1212121221220", "1121123221222", "1121121212220", "1212112121220",
				"2121231212121", "2221211212120", "1221212121210", "2123221212121", "2121212212120", "1211212232212", "1211212122210",
				"2121121212220", "1212132112212", "2212112112210", "2212211212120", "1221412121212", "1212122121210", "2112212122120",
				"1231212122212", "1211212122210", "2121123122122", "2121121122120", "2212112112120", "2212231212112", "2122121212120",
				"1212122121210", "2132122122121", "2112121222120", "1211212322122", "1211211221220", "2121121121220", "2122132112122",
				"1221212121120", "2121221212110", "2122321221212", "1121212212210", "2112121221220", "1231211221222", "1211211212220",
				"1221123121221", "2221121121210", "2221212112120", "1221241212112", "1212212212120", "1121212212210", "2114121212221",
				"2112112122210", "2211211412212", "2211211212120", "2212121121210", "2212214112121", "2122122121120", "1212122122120",
				"1121412122122", "1121121222120", "2112112122120", "2231211212122", "2121211212120", "2212121321212", "2122121121210",
				"2122121212120", "1212142121212", "1211221221220", "1121121221220", "2114112121222", "1212112121220", "2121211232122",
				"1221211212120", "1221212121210", "2121223212121", "2121212212120", "1211212212210", "2121321212221", "2121121212220",
				"1212112112210", "2223211211221", "2212211212120", "1221212321212", "1212122121210", "2112212122120", "1211232122212",
				"1211212122210", "2121121122210", "2212312112212", "2212112112120", "2212121232112", "2122121212110", "2212122121210",
				"2112124122121", "2112121221220", "1211211221220", "2121321122122", "2121121121220", "2122112112322", "1221212112120",
				"1221221212110", "2122123221212", "1121212212210", "2112121221220", "1211231212222", "1211211212220", "1221121121220",
				"1223212112121", "2221212112120", "1221221232112", "1212212122120", "1121212212210", "2112132212221", "2112112122210",
				"2211211212210", "2221321121212", "2212121121210", "2212212112120", "1232212122112", "1212122122120", "1121212322122",
				"1121121222120", "2112112122120", "2211231212122", "2121211212120", "2122121121210", "2124212112121", "2122121212120",
				"1212121223212", "1211212221220", "1121121221220", "2112132121222", "1212112121220", "2121211212120", "2122321121212",
				"1221212121210", "2121221212120", "1232121221212", "1211212212210", "2121123212221", "2121121212220", "1212112112220",
				"1221231211221", "2212211211220", "1212212121210", "2123212212121", "2112122122120", "1211212322212", "1211212122210",
				"2121121122120", "2212114112122", "2212112112120", "2212121211210", "2212232121211", "2122122121210", "2112122122120",
				"1231212122212", "1211211221220", "2121121321222", "2121121121220", "2122112112120", "2122141211212", "1221221212110",
				"2121221221210", "2114121221221" };
		int[] currentDate = new int[162];
		int[] MonthAll = { 31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		int dateAll = 0;
		int currentDateAll = 0;
		int currentYear = 0;

		int tempDate = 0;
		int tempDate0 = 0;
		int m1 = 0;
		int m2 = 0;
		int JCOUNT = 0;

		for (int i = 0; i < 162; i++) {
			currentDate[i] = 0;
			for (int j = 0; j <= 12; j++) {
				if (MonthData[i].substring(j, j + 1).equals("1") || MonthData[i].substring(j, j + 1).equals("3"))
					currentDate[i] = currentDate[i] + 29;
				else if (MonthData[i].substring(j, j + 1).equals("2") || MonthData[i].substring(j, j + 1).equals("4"))
					currentDate[i] = currentDate[i] + 30;
			}
		}
		dateAll = 1880 * 365 + 1880 / 4 - 1880 / 100 + 1880 / 400 + 30;
		currentYear = year - 1;
		currentDateAll = currentYear * 365 + currentYear / 4 - currentYear / 100 + currentYear / 400;

		if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
			MonthAll[1] = 29;
		else
			MonthAll[1] = 28;

		if (day <= MonthAll[month - 1])

			for (int i = 0; i < month - 1; i++) {
				currentDateAll = currentDateAll + MonthAll[i];
			}
		currentDateAll = currentDateAll + day;

		tempDate = currentDateAll - dateAll + 1;

		tempDate0 = currentDate[0];
		int thisdate = 0;
		while (tempDate > tempDate0) {
			tempDate0 = tempDate0 + currentDate[thisdate + 1];
			thisdate++;
		}
		year = thisdate + 1881;

		tempDate0 = tempDate0 - currentDate[thisdate];
		tempDate = tempDate - tempDate0;

		if (MonthData[thisdate].substring(12).equals("0"))
			JCOUNT = 11;
		else
			JCOUNT = 12;
		m2 = 0;
		for (int j = 0; j <= JCOUNT; j++) {
			if (Integer.parseInt(MonthData[thisdate].substring(j, j + 1)) <= 2) {
				m2 = m2 + 1;
				m1 = Integer.parseInt(MonthData[thisdate].substring(j, j + 1)) + 28;
			} else
				m1 = Integer.parseInt(MonthData[thisdate].substring(j, j + 1)) + 26;
			if (tempDate <= m1) {
				break;
			}
			tempDate = tempDate - m1;
		}
		month = m2;
		day = tempDate;
		Hashtable LunDate = new Hashtable();

		LunDate.put(new String("month"), new Integer(month));
		LunDate.put(new String("day"), new Integer(day));
		return LunDate;
	}

	/**
	 * 음력을 양력으로 변환한다.<br>
	 * get 메소드를 이용하여 변환된 값을 얻을수 있다.<br>
	 * get("month") - 양력으로 변환된 월<br>
	 * get("day") - 양력으로 변환된 날짜
	 *
	 * @param ssyear
	 *            년
	 * @param ssmonth
	 *            월
	 * @param ssday
	 *            일
	 * @param yunweol
	 *            윤달 여부 0이면 윤달이 아니고 1이면 윤달이다.
	 * @return Hashtable 변환된 양력 날짜를 포함하는 Hashtable
	 */
	public static Hashtable lunarToSolar(int ssyear, int ssmonth, int ssday, String yunweol) {

		String lunar[] = { "1212122322121", "1212121221220", "1121121222120", "2112132122122", "2112112121220", "2121211212120", "2212321121212",
				"2122121121210", "2122121212120", "1232122121212", "1212121221220", "1121123221222", "1121121212220", "1212112121220",
				"2121231212121", "2221211212120", "1221212121210", "2123221212121", "2121212212120", "1211212232212", "1211212122210",
				"2121121212220", "1212132112212", "2212112112210", "2212211212120", "1221412121212", "1212122121210", "2112212122120",
				"1231212122212", "1211212122210", "2121123122122", "2121121122120", "2212112112120", "2212231212112", "2122121212120",
				"1212122121210", "2132122122121", "2112121222120", "1211212322122", "1211211221220", "2121121121220", "2122132112122",
				"1221212121120", "2121221212110", "2122321221212", "1121212212210", "2112121221220", "1231211221222", "1211211212220",
				"1221123121221", "2221121121210", "2221212112120", "1221241212112", "1212212212120", "1121212212210", "2114121212221",
				"2112112122210", "2211211412212", "2211211212120", "2212121121210", "2212214112121", "2122122121120", "1212122122120",
				"1121412122122", "1121121222120", "2112112122120", "2231211212122", "2121211212120", "2212121321212", "2122121121210",
				"2122121212120", "1212142121212", "1211221221220", "1121121221220", "2114112121222", "1212112121220", "2121211232122",
				"1221211212120", "1221212121210", "2121223212121", "2121212212120", "1211212212210", "2121321212221", "2121121212220",
				"1212112112210", "2223211211221", "2212211212120", "1221212321212", "1212122121210", "2112212122120", "1211232122212",
				"1211212122210", "2121121122210", "2212312112212", "2212112112120", "2212121232112", "2122121212110", "2212122121210",
				"2112124122121", "2112121221220", "1211211221220", "2121321122122", "2121121121220", "2122112112322", "1221212112120",
				"1221221212110", "2122123221212", "1121212212210", "2112121221220", "1211231212222", "1211211212220", "1221121121220",
				"1223212112121", "2221212112120", "1221221232112", "1212212122120", "1121212212210", "2112132212221", "2112112122210",
				"2211211212210", "2221321121212", "2212121121210", "2212212112120", "1232212122112", "1212122122120", "1121212322122",
				"1121121222120", "2112112122120", "2211231212122", "2121211212120", "2122121121210", "2124212112121", "2122121212120",
				"1212121223212", "1211212221220", "1121121221220", "2112132121222", "1212112121220", "2121211212120", "2122321121212",
				"1221212121210", "2121221212120", "1232121221212", "1211212212210", "2121123212221", "2121121212220", "1212112112220",
				"1221231211221", "2212211211220", "1212212121210", "2123212212121", "2112122122120", "1211212322212", "1211212122210",
				"2121121122120", "2212114112122", "2212112112120", "2212121211210", "2212232121211", "2122122121210", "2112122122120",
				"1231212122212", "1211211221220" };

		int lday[] = { 31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		int m1 = -1;
		int td = 0;
		int n2 = 0;
		int m2 = 0;
		int y = 0;
		int w2 = 0;
		;
		int syear = 0;
		int smonth = 0;
		int sday = 0;
		int syear1 = 0;
		int smonth1 = 0;
		int sday1 = 0;
		int youn = 0;
		String tt = "0";
		int ty = 0;
		String year = null;
		String month = null;
		String luday = null;

		syear = ssyear;
		smonth = ssmonth;
		sday = ssday;

		if (syear != 1881) {
			m1 = syear - 1882;

			for (int i = 0; i <= m1; i++) {
				for (int j = 0; j <= 12; j++) {

					tt = lunar[i].substring(j, 1 + j);
					ty = Integer.parseInt(tt);
					td = td + ty;
				}
				if (Integer.parseInt(lunar[i].substring(12, 13)) == 0) {
					td += 336;
				} else {
					td += 362;
				}
			}
		}

		m1++;
		n2 = smonth - 1;
		m2 = -1;
		while (true) {
			m2++;
			if (Integer.parseInt(lunar[m1].substring(m2, 1 + m2)) > 2) {
				td += 26 + Integer.parseInt(lunar[m1].substring(m2, 1 + m2));
				n2++;
			} else {
				if (m2 == n2) {
					break;
				} else {
					td += 28 + Integer.parseInt(lunar[m1].substring(m2, 1 + m2));
				}
			}
		}
		// 윤달일 경우
		if (yunweol.equals("1")) {
			td += 28 + Integer.parseInt(lunar[m1].substring(m2, 1 + m2));
		}

		td += sday + 29;
		m1 = 1880;
		while (true) {
			m1++;
			if ((m1 / 400. - (int) (m1 / 400)) * 400 == 0 || (m1 / 100. - (int) (m1 / 100)) * 100 != 0 && (m1 / 4. - (int) (m1 / 4)) * 4 == 0) {
				m2 = 366;
			} else {
				m2 = 365;
			}
			if (td < m2) {
				break;
			}
			td -= m2;
		}

		syear1 = m1;
		lday[1] = m2 - 337;
		m1 = 0;

		while (true) {
			m1++;
			if (td <= lday[m1 - 1]) {
				break;
			}
			td -= lday[m1 - 1];
		}

		smonth1 = m1;
		sday1 = td;
		y = syear1 - 1;

		td = y * 365 + (int) (y / 4) - (int) (y / 100) + (int) (y / 400);

		if ((syear1 / 400. - (int) (syear1 / 400)) * 400 == 0 || (syear1 / 100. - (int) (syear1 / 100)) * 100 != 0
				&& (syear1 / 4. - (int) (syear1 / 4)) * 4 == 0) {
			lday[1] = 29;
		} else {
			lday[1] = 28;
		}

		for (int i = 0; i <= smonth1 - 2; i++) {
			td += lday[i];
		}
		td += sday1;

		w2 = td;
		while (true) {
			if (w2 < 7) {
				break;
			}
			w2 -= 7;
		}

		year = Integer.toString(syear1);

		Hashtable hash = new Hashtable();

		month = Integer.toString(smonth1);
		luday = Integer.toString(sday1);

		hash.put(new String("year"), new Integer(syear1));
		hash.put(new String("month"), new Integer(smonth1));
		hash.put(new String("day"), new Integer(sday1));
		return hash;

	}

	/**
	 * getStringDateTime.
	 *
	 * @param date string 형태의 날짜 data (yyyymmddhh24miss)
	 * @param type String
	 * @return the string date time
	 */
	public static String getStringDateTime(String date, String type) {
		String ret = "";
		String time = "";
		try {
			ret = getStringDate(date, type);
			time = getStringTime(date, ":");
			ret = ret + " " + time;
		} catch (Exception e) {
			return new String("");
		}
		return ret;
	}

	/**
	 * getStringDate.
	 *
	 * @param date string 형태의 날짜 data (yyyymmddhh24miss)
	 * @param type String
	 * @return String yyyy년 mm월 dd일
	 */
	public static String getStringDate(String date, String type) {
		String ret;
		try {
			if (date.length() >= 8) {
				if (type.equals("kor")) {
					ret = date.substring(0, 4) + "년 " + Integer.parseInt(date.substring(4, 6)) + "월 " + Integer.parseInt(date.substring(6, 8)) + "일";
				} else {
					ret = date.substring(0, 4) + type + date.substring(4, 6) + type + date.substring(6, 8);
				}
			} else
				return new String("");
		} catch (Exception e) {
			return new String("");
		}
		return ret;
	}

	/**
	 * getStringTime.
	 *
	 * @param date string 형태의 날짜 data (yyyymmddhh24miss)
	 * @param type String
	 * @return String yyyy년 mm월 dd일
	 */
	public static String getStringTime(String date, String type) {
		String ret;
		// this.logger.debug("date = "+date);
		try {
			if (date.length() >= 14) {
				if (type.equals("kor")) {
					ret = date.substring(8, 10) + "시 " + date.substring(10, 12) + "분 " + date.substring(12, 14) + "초";
				} else {
					ret = date.substring(8, 10) + type + date.substring(10, 12) + type + date.substring(12, 14);
				}
			} else
				return new String("");
		} catch (Exception e) {
			return new String("");
		}
		return ret;
	}

	/**
	 * 입력받은 Date 오브젝트를 특정한 포멧 형식의 String 으로 만들어 리턴.
	 *
	 * @param d Date
	 * @param format String
	 * @return the string
	 */
	public static String dateToString(Date d, String format) {
		String ch = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			ch = sdf.format(d);
		} catch (Exception dfdf) {
		}
		return ch;
	}

	/**
	 * getStringTime.
	 *
	 * @param resultset ResultSet
	 * @param i int
	 * @return Date
	 * @throws Exception exception
	 */
	public static Date sqlDateToUtilDate(ResultSet resultset, int i) throws Exception {
		Date date = null;
		try {

			if (resultset.getDate(i) != null) {
				java.sql.Date date1 = resultset.getDate(i);
				java.sql.Time time = resultset.getTime(i);
				if (time != null)
					date = new Date(date1.getTime() + (time.getTime() + 32400000L));
				else
					date = new Date(date1.getTime());
			}
		} catch (Exception exception) {
			logger.error(exception.getMessage());
		}
		return date;
	}

	/**
	 * 입련된 시간의 값이 5일을 지났는지 체크한다.
	 *
	 * @param currentTime String
	 * @return true = "5일전" , false = "5일후"
	 */
	public static boolean compareTime(String currentTime) {

		Date date = StringtoDate(currentTime);
		long today = System.currentTimeMillis();
		long insert_time = date.getTime();
		long result_day = (today - insert_time) / (1000 * 60 * 60 * 24);
		if (result_day < 4) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Compare.
	 *
	 * @param type String
	 * @param tm String
	 * @return the long
	 */
	public static long compare(String type, String tm) {
		return compare(type, getDate("yyyy-MM-dd HH:mm:ss"), tm);
	}

	/**
	 * Compare.
	 *
	 * @param type String
	 * @param tm1 String
	 * @param tm2 String
	 * @return the long
	 */
	public static long compare(String type, String tm1, String tm2) {
		long n = 0;

		Date date1 = StringtoDate(tm1);
		Date date2 = StringtoDate(tm2);
		long result_time = (date1.getTime() - date2.getTime());

		n = (result_time / (1000 * 60));

		return n;
	}

	/**
	 * 현재 날짜에서 baseNum 전후의 일자.
	 *
	 * @param date yyyymmdd
	 * @param baseNum int
	 * @return Unixtimestamp
	 */
	public static long baseDate(String date, int baseNum) {
		return baseDate(StringtoDate(date), baseNum);
	}

	/**
	 * 현재 날짜에서 baseNum 전후의 일자.
	 *
	 * @param date Date
	 * @param baseNum int
	 * @return Unixtimestamp
	 */
	public static long baseDate(Date date, int baseNum) {
		long currentDate = date.getTime();
		long resultDate = currentDate + baseNum * (1000 * 60 * 60 * 24);
		return resultDate;
	}

	/**
	 * 입련된 시간의 값이 5일을 지났는지 체크한다.
	 *
	 * @param date Date
	 * @return true = "5일전" , false = "5일후"
	 */
	public static boolean compareTime(Date date) {
		long today = System.currentTimeMillis();
		long insert_time = date.getTime();
		long result_day = (today - insert_time) / (1000 * 60 * 60 * 24);
		if (result_day < 4) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 두 Date의 차를 구한다.
	 *
	 * @param fromDate Date
	 * @param toDate Date
	 * @return the string
	 */
	public static String dateDifference(Date fromDate, Date toDate) {
		return Long.toString((toDate.getTime() - fromDate.getTime()) / (1000 * 60 * 60 * 24));
	}

	/**
	 * 두 Date의 차를 구한다.
	 * 
	 * @param date String
	 * @return the long
	 * <p>예) currentDate : 20110926
	 * dateDifference(20110924) : 2
	 * dateDifference(20110925) : 1
	 * dateDifference(20110926) : 0
	 * dateDifference(20110927) : 0
	 * dateDifference(20110928) : -1
	 * dateDifference(20111027) : -30
	 * dateDifference(20111028) : -31
	 * </p>
	 */
	public static long dateDifference(String date){
		return Long.parseLong(dateDifference(StringtoDate(date),new Date()));
	}

	/**
	 * 지정된 날짜만큼 더하거나 뺀 날짜를 구한다.
	 *
	 * @param date (yyyyMMdd)
	 * @param days int
	 * @return the string
	 */
	public static String dateAdd(String date, int days) {
	    Calendar cal = Calendar.getInstance();
	    int year=0, month=0, day=0;
	    try {
			year   = Integer.parseInt(date.substring(0,4));
		    month  = Integer.parseInt(date.substring(4,6));
		    day    = Integer.parseInt(date.substring(6,8));
	    } catch (Exception e) {}
		cal.set(year, month-1, day+days);
		return getDate(cal.getTime(), "yyyyMMdd");
	}

	/**
	 * 포멧에 따른 현재 날짜.
	 *
	 * @param format String
	 * @return the current date
	 */
	public static String getCurrentDate(String format) {
		return getDate(new Date(), format);
	}

	/**
	 * Gets the current date.
	 *
	 * @return the current date - format "yyyyMMdd"
	 */
	public static String getCurrentDate() {
		return getDate(new Date(), "yyyyMMdd");
	}

	/**
	 * Gets the current time.
	 *
	 * @return the current time - format "HHmmss"
	 */
	public static String getCurrentTime() {
		return getDate(new Date(), "HHmmss");
	}

	/**
	 * Gets the date.
	 *
	 * @param day int
	 * @return the date - format "yyyyMMdd"
	 */
	public static String getDate(int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, day);
		return getDate(calendar.getTime(), "yyyyMMdd");
	}

	/**
	 * Gets the date.
	 *
	 * @param day int
	 * @param format String
	 * @return the date
	 */
	public static String getDate(int day, String format) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, day);
		return getDate(calendar.getTime(), format);
	}

	/**
	 * 년도를 선택 할 수 있는 select Box를 만들어준다.
	 *
	 * @param year String
	 * @return the string
	 */
	public static String makeYearSelectBox(String year) {
		StringBuffer str = new StringBuffer();
		String nowYear = DateUtil.getDate("yyyy");

		year = StringUtil.nullToString(year);
		int tmpYear = Integer.parseInt(nowYear);

		str.append("<option value=00 >선택</option>");

		for (int i = tmpYear - 2; i <= tmpYear + 3; i++) {
			str.append("<option value=" + i);
			if (!year.equals("")) {
				if (Integer.parseInt(year) == i) {
					str.append(" selected");
				}
			} else {
				if (tmpYear == i) {
					str.append(" selected");
				}
			}
			str.append(">" + i + "</option>");

		}

		return str.toString();
	}

	/**
	 * 월을 선택 할 수 있는 select Box를 만들어준다.
	 *
	 * @param month String
	 * @return the string
	 */
	public static String makeMonthSelectBox(String month) {
		StringBuffer str = new StringBuffer();
		DecimalFormat df = new DecimalFormat("00");
		StringUtil.nullToString(month);

		String nowMonth = DateUtil.getDate("MM");
		int tmpMonth = Integer.parseInt(nowMonth);

		str.append("<option value=\"\" >선택</option>");

		for (int i = 1; i <= 12; i++) {
			str.append("<option value=" + df.format(i));
			if (month.equals(df.format(i))) {
				str.append(" selected");
			} else {
				if (month.equals("") && tmpMonth == i) {
					str.append(" selected");
				}
			}
			str.append(">" + df.format(i) + "</option>");
		}
		return str.toString();
	}


	/**
	 * 오늘 시작시간을 가져온다.
	 *
	 * @return the today start
	 */
	public static Date getTodayStart(){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * 오늘 종료시간을 가져온다.
	 *
	 * @return the today end
	 */
	public static Date getTodayEnd(){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}

	/**
	 * GMT 날짜.
	 *
	 * @return the gMT date
	 */
	public static String getGMTDate(){
		long time = System.currentTimeMillis();
		Calendar gmt = Calendar.getInstance();
		gmt.setTimeInMillis(time);

		TimeZone.setDefault(TimeZone.getTimeZone("GMT"));
		Date date = new Date(time);
		return date.toGMTString();
	}

	/**
	 * Gets the rss format date.
	 *
	 * @param date Date
	 * @return the rss format date
	 */
	public static String getRssFormatDate(Date date){
	    SimpleDateFormat formatter= new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z",Locale.ENGLISH);
	    return formatter.format(date);
	}

	/**
	 * <p>두 날짜간 몇분의 간격이 있는지 계산한다.</p>
	 * <p>예를들어 (2009-02-15 10:00) - (2009-02-15 11:00)는 -60분이다.</p>
	 * <p>예2 (2009-02-15 10:00) - (2009-02-16 10:00)는 -1440(60*24)분이다.</p>
	 * <p>초는 무시되고 오직 분만으로 계산된다.</p>
	 * <p>이 메서드는 로컬 타임존({@link TimeZone#getDefault()})의 값에 기준하여 변환한다.</p>
	 *
	 * @param d1 날짜1
	 * @param d2 날짜2
	 * @return the int
	 */
	public static int minDiff(Date d1, Date d2) {
		return minDiff(d1, d2, TimeZone.getDefault());
	}
	
	public static int minDiff(long d1, long d2) {
		return minDiff(d1, d2, TimeZone.getDefault());
	}

	/**
	 * <p>두 날짜간 몇분의 간격이 있는지 계산한다.</p>
	 * <p>예를들어 (2009-02-15 10:00) - (2009-02-15 11:00)는 -60분이다.</p>
	 * <p>예2 (2009-02-15 10:00) - (2009-02-16 10:00)는 -1440(60*24)분이다.</p>
	 * <p>초는 무시되고 오직 분만으로 계산된다.</p>
	 * <p>이 메서드는 로컬 타임존({@link TimeZone#getDefault()})의 값에 기준하여 변환한다.</p>
	 *
	 * @param d1 날짜1
	 * @param d2 날짜2
	 * @param tz 날짜 계산의 기준이 되는 타임존
	 * @return the int
	 */
	public static int minDiff(Date d1, Date d2, TimeZone tz) {
		return (int)(getMinStamp(d1, tz) - getMinStamp(d2, tz));
	}
	
	public static int minDiff(long d1, long d2, TimeZone tz) {
		return (int)(getMinStamp(d1, tz) - getMinStamp(d2, tz));
	}

	/**
	 * <p>두 날짜간 몇일의 간격이 있는지 계산한다.</p>
	 * <p>예를들어 (2009-01-01일) - (2009-01-02일)는 -1일이다.</p>
	 * <p>날짜의 시간은 무시되고 오직 날짜만으로 계산된다.</p>
	 * <p>이 메서드는 로컬 타임존({@link TimeZone#getDefault()})의 값에 기준하여 변환한다.</p>
	 *
	 * @param d1 날짜1
	 * @param d2 날짜2
	 * @return the int
	 */
	public static int dateDiff(Date d1, Date d2) {
		return dateDiff(d1, d2, TimeZone.getDefault());
	}

	/**
	 * <p>두 날짜간 몇일의 간격이 있는지 계산한다.</p>
	 * <p>예를들어 (2009-01-01일) - (2009-01-02일)는 -1일이다.</p>
	 * <p>날짜의 시간은 무시되고 오직 날짜만으로 계산된다.</p>
	 *
	 * @param d1 날짜1
	 * @param d2 날짜2
	 * @param tz 날짜 계산의 기준이 되는 타임존
	 * @return the int
	 */
	public static int dateDiff(Date d1, Date d2, TimeZone tz) {
		return (int)(getDateStamp(d1, tz) - getDateStamp(d2, tz));
	}

	/**
	 * <p>두 날짜간 몇주의 간격이 있는지 계산한다.</p>
	 * <p>예를들어 (2009-02-15 ~ 2009-02-21) - (2009-02-22 ~ 2009-02-28)는 -1주이다.</p>
	 * <p>날짜와 시간은 무시되고 오직 주만으로 계산된다.</p>
	 * <p>이 메서드는 로컬 타임존({@link TimeZone#getDefault()})의 값에 기준하여 변환한다.</p>
	 *
	 * @param d1 날짜1
	 * @param d2 날짜2
	 * @return the int
	 */
	public static int weekDiff(Date d1, Date d2) {
		return weekDiff(d1, d2, TimeZone.getDefault());
	}

	/**
	 * <p>두 날짜간 몇주의 간격이 있는지 계산한다.</p>
	 * <p>예를들어 (2009-02-15 ~ 2009-02-21) - (2009-02-22 ~ 2009-02-28)는 -1주이다.</p>
	 * <p>날짜와 시간은 무시되고 오직 주만으로 계산된다.</p>
	 *
	 * @param d1 날짜1
	 * @param d2 날짜2
	 * @param tz 날짜 계산의 기준이 되는 타임존
	 * @return the int
	 */
	public static int weekDiff(Date d1, Date d2, TimeZone tz) {
		return (int)(getWeekStamp(d1, tz) - getWeekStamp(d2, tz));
	}

	/**
	 * <p>두 날짜간 몇개월의 간격이 있는지 계산한다.</p>
	 * <p>예를들어 (2009-02-01 ~ 2009-02-28) - (2009-03-01 ~ 2009-03-31)는 -1개월이다.</p>
	 * <p>날짜와 시간은 무시되고 오직 연도와 월만으로 계산된다.</p>
	 * <p>이 메서드는 로컬 타임존({@link TimeZone#getDefault()})의 값에 기준하여 변환한다.</p>
	 *
	 * @param d1 날짜1
	 * @param d2 날짜2
	 * @return the int
	 */
	public static int monthDiff(Date d1, Date d2) {
		return monthDiff(d1, d2, TimeZone.getDefault());
	}

	/**
	 * <p>두 날짜간 몇개월의 간격이 있는지 계산한다.</p>
	 * <p>예를들어 (2009-02-01 ~ 2009-02-28) - (2009-03-01 ~ 2009-03-31)는 -1개월이다.</p>
	 * <p>날짜와 시간은 무시되고 오직 연도와 월만으로 계산된다.</p>
	 *
	 * @param d1 날짜1
	 * @param d2 날짜2
	 * @param tz 날짜 계산의 기준이 되는 타임존
	 * @return the int
	 */
	public static int monthDiff(Date d1, Date d2, TimeZone tz) {
		return monthDiff(d1, d2, Calendar.getInstance(tz));
	}

	/**
	 * <p>두 날짜간 몇개월의 간격이 있는지 계산한다.</p>
	 * <p>예를들어 (2009-02-01 ~ 2009-02-28) - (2009-03-01 ~ 2009-03-31)는 -1개월이다.</p>
	 * <p>날짜와 시간은 무시되고 오직 연도와 월만으로 계산된다.</p>
	 * <p>이 메쏘드 수행 후 캘린더 객체의 값(날짜)은 변경된다.</p>
	 *
	 * @param d1 날짜1
	 * @param d2 날짜2
	 * @param cal 계산에 사용할 캘린더 객체
	 * @return the int
	 */
	public static int monthDiff(Date d1, Date d2, Calendar cal) {
		return (int)(getMonthStamp(d1, cal) - getMonthStamp(d2, cal));
	}

	/**
	 * <p>1970-01-01 부터 경과한 날짜 수를 리턴한다.</p>
	 * <p>1970-01-01일은 0일이며 1969-12-31일은 -1일이다.</p>
	 * <p>이 메서드는 로컬 타임존({@link TimeZone#getDefault()})의 값에 기준하여 변환한다.</p>
	 *
	 * @param date 날짜
	 * @return the min stamp
	 * @see TimeZone#getDefault()
	 */
	public static long getMinStamp(Date date) {
		return getMinStamp(date, TimeZone.getDefault());
	}

	/**
	 * <p>1970-01-01 부터 경과한 날짜 수를 리턴한다.</p>
	 * <p>1970-01-01일은 0일이며 1969-12-31일은 -1일이다.</p>
	 *
	 * @param date 날짜
	 * @param tz 날짜 계산의 기준이 되는 타임존
	 * @return the min stamp
	 */
	public static long getMinStamp(Date date, TimeZone tz) {
		return (long)Math.floor((date.getTime() + tz.getRawOffset())/60000.0);
	}
	
	public static long getMinStamp(long date, TimeZone tz) {
		return (long)Math.floor((date + tz.getRawOffset())/60000.0);
	}

	/**
	 * <p>1970-01-01 부터 경과한 날짜 수를 리턴한다.</p>
	 * <p>1970-01-01일은 0일이며 1969-12-31일은 -1일이다.</p>
	 * <p>이 메서드는 로컬 타임존({@link TimeZone#getDefault()})의 값에 기준하여 변환한다.</p>
	 *
	 * @param date 날짜
	 * @return the date stamp
	 * @see TimeZone#getDefault()
	 */
	public static long getDateStamp(Date date) {
		return getDateStamp(date, TimeZone.getDefault());
	}

	/**
	 * <p>1970-01-01 부터 경과한 날짜 수를 리턴한다.</p>
	 * <p>1970-01-01일은 0일이며 1969-12-31일은 -1일이다.</p>
	 *
	 * @param date 날짜
	 * @param tz 날짜 계산의 기준이 되는 타임존
	 * @return the date stamp
	 */
	public static long getDateStamp(Date date, TimeZone tz) {
		return (long)Math.floor((date.getTime() + tz.getRawOffset())/86400000.0);
	}

	/**
	 * <p>1970-01-01 부터 경과한 날짜 수를 해당 날짜의 {@link java.util.Date}객체로 변환한다.</p>
	 * <p>{@link java.util.Date}객체의 시간은 새벽 0시로 설정 된다.</p>
	 * <p>이 메서드는 로컬 타임존({@link TimeZone#getDefault()})의 값에 기준하여 변환한다.</p>
	 *
	 * @param dateStamp 1970-01-01 부터 경과한 날짜 수
	 * @return the date from date stamp
	 * @see TimeZone#getDefault()
	 */
	public static Date getDateFromDateStamp(long dateStamp) {
		return getDateFromDateStamp(dateStamp, TimeZone.getDefault());
	}

	/**
	 * <p>1970-01-01 부터 경과한 날짜 수를 해당 날짜의 {@link java.util.Date}객체로 변환한다.</p>
	 * <p>{@link java.util.Date}객체의 시간은 새벽 0시로 설정 된다.</p>
	 *
	 * @param dateStamp 1970-01-01 부터 경과한 날짜 수
	 * @param tz 날짜 계산의 기준이 되는 타임존
	 * @return the date from date stamp
	 */
	public static Date getDateFromDateStamp(long dateStamp, TimeZone tz) {
		return new Date((dateStamp * 86400000L) - tz.getRawOffset());
	}

	/**
	 * <p>1970-01-01 부터 경과한 주 수를 리턴한다.</p>
	 * <p>1970-01-01이 포함 된 주(1969-12-28 ~ 1970-01-03)는 0주이며,
	 * 1969-12-21 ~ 1970-01-27는 -1주이다.</p>
	 * <p>힌트: 1970-01-01은 목요일이며 4일을 빼주면 일요일이 된다.</p>
	 * <p>이 메서드는 로컬 타임존({@link TimeZone#getDefault()})의 값에 기준하여 변환한다.</p>
	 *
	 * @param date 날짜
	 * @return the week stamp
	 * @see TimeZone#getDefault()
	 */
	public static long getWeekStamp(Date date) {
		return getWeekStamp(date, TimeZone.getDefault());
	}

	/**
	 * <p>1970-01-01 부터 경과한 주 수를 리턴한다.</p>
	 * <p>1970-01-01이 포함 된 주(1969-12-28 ~ 1970-01-03)는 0주이며,
	 * 1969-12-21 ~ 1970-01-27는 -1주이다.</p>
	 * <p>힌트: 1970-01-01은 목요일이며 4일을 빼주면 일요일이 된다.</p>
	 *
	 * @param date 날짜
	 * @param tz 날짜 계산의 기준이 되는 타임존
	 * @return the week stamp
	 */
	public static long getWeekStamp(Date date, TimeZone tz) {
		return (long)Math.floor((getDateStamp(date, tz) + 4) / 7.0);
	}

	/**
	 * <p>1970-01-01 부터 경과한 주 수를 해당 주의 시작일을 나타내는 {@link java.util.Date}객체로 변환한다.</p>
	 * <p>{@link java.util.Date}객체의 시간은 새벽 0시로 설정 된다.</p>
	 * <p>이 메서드는 로컬 타임존({@link TimeZone#getDefault()})의 값에 기준하여 변환한다.</p>
	 *
	 * @param weekStamp 1970-01-01 부터 경과한 주 수
	 * @return the date from week stamp
	 * @see TimeZone#getDefault()
	 */
	public static Date getDateFromWeekStamp(long weekStamp) {
		return getDateFromWeekStamp(weekStamp, TimeZone.getDefault());
	}

	/**
	 * <p>1970-01-01 부터 경과한 주 수를 해당 주의 시작일을 나타내는 {@link java.util.Date}객체로 변환한다.</p>
	 * <p>{@link java.util.Date}객체의 시간은 새벽 0시로 설정 된다.</p>
	 *
	 * @param weekStamp 1970-01-01 부터 경과한 주 수
	 * @param tz 날짜 계산의 기준이 되는 타임존
	 * @return the date from week stamp
	 */
	public static Date getDateFromWeekStamp(long weekStamp, TimeZone tz) {
		return getDateFromDateStamp((weekStamp * 7) - 4, tz);
	}

	/**
	 * <p>1970-01-01 부터 경과한 월 수를 리턴한다.</p>
	 * <p>1970-01 월이 0월이고 1969-12월이 -1월이다.</p>
	 * <p>이 메서드는 로컬 타임존({@link TimeZone#getDefault()})의 값에 기준하여 변환한다.</p>
	 *
	 * @param date 날짜
	 * @return the month stamp
	 * @see TimeZone#getDefault()
	 */
	public static int getMonthStamp(Date date) {
		return getMonthStamp(date, TimeZone.getDefault());
	}

	/**
	 * <p>1970-01-01 부터 경과한 월 수를 리턴한다.</p>
	 * <p>1970-01 월이 0월이고 1969-12월이 -1월이다.</p>
	 *
	 * @param date 날짜
	 * @param tz 날짜 계산의 기준이 되는 타임존
	 * @return the month stamp
	 */
	public static int getMonthStamp(Date date, TimeZone tz) {
		return getMonthStamp(date, Calendar.getInstance(tz));
	}

	/**
	 * <p>1970-01-01 부터 경과한 월 수를 리턴한다.</p>
	 * <p>1970-01 월이 0월이고 1969-12월이 -1월이다.</p>
	 * <p>이 메쏘드 수행 후 캘린더 객체의 값(날짜)은 변경된다.</p>
	 *
	 * @param date 날짜
	 * @param cal 날짜 계산에 사용될 캘린더 객체
	 * @return the month stamp
	 */
	public static int getMonthStamp(Date date, Calendar cal) {
		cal.setTime(date);
		return (cal.get(Calendar.YEAR) - 1970) * 12 + cal.get(Calendar.MONTH);
	}

	/**
	 * <p>1970-01-01 부터 경과한 월 수를 해당 월의 시작일(1일)을 나타내는 {@link java.util.Date}객체로 변환한다.</p>
	 * <p>{@link java.util.Date}객체의 시간은 새벽 0시로 설정 된다.</p>
	 * <p>이 메서드는 로컬 타임존({@link TimeZone#getDefault()})의 값에 기준하여 변환한다.</p>
	 *
	 * @param monthStamp 1970-01-01 부터 경과한 월 수
	 * @return the date from month stamp
	 * @see TimeZone#getDefault()
	 */
	public static Date getDateFromMonthStamp(long monthStamp) {
		return getDateFromMonthStamp(monthStamp, TimeZone.getDefault());
	}

	/**
	 * <p>1970-01-01 부터 경과한 월 수를 해당 월의 시작일(1일)을 나타내는 {@link java.util.Date}객체로 변환한다.</p>
	 * <p>{@link java.util.Date}객체의 시간은 새벽 0시로 설정 된다.</p>
	 *
	 * @param monthStamp 1970-01-01 부터 경과한 월 수
	 * @param tz 날짜 계산의 기준이 되는 타임존
	 * @return the date from month stamp
	 */
	public static Date getDateFromMonthStamp(long monthStamp, TimeZone tz) {
		return getDateFromMonthStamp(monthStamp, Calendar.getInstance(tz));
	}

	/**
	 * <p>1970-01-01 부터 경과한 월 수를 해당 월의 시작일(1일)을 나타내는 {@link java.util.Date}객체로 변환한다.</p>
	 * <p>{@link java.util.Date}객체의 시간은 새벽 0시로 설정 된다.</p>
	 * <p>이 메쏘드 수행 후 캘린더 객체의 값(날짜)은 변경된다.</p>
	 *
	 * @param monthStamp 1970-01-01 부터 경과한 월 수
	 * @param cal 날짜 계산에 사용될 캘린더 객체
	 * @return the date from month stamp
	 */
	public static Date getDateFromMonthStamp(long monthStamp, Calendar cal) {
		cal.clear();
		cal.set(Calendar.YEAR, (int)(monthStamp / 12 + 1970));
		cal.set(Calendar.MONTH, (int)(monthStamp % 12));
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	/*
	 * 양력/음력 변환 관련
	 * .NET 소스 Calendar.cs 를 포팅
	 */

	/**
	 * <p>음력 데이터.</p>
	 * <ul>
	 * 	<li>1: 평달 - 작은달</li>
	 * 	<li>2: 평달 - 큰달</li>
	 * 	<li>3: 윤달이 있는 달 - 평달이 작고 윤달도 작으면</li>
	 * 	<li>4: 윤달이 있는 달 - 평달이 작고 윤달이 크면</li>
	 * 	<li>5: 윤달이 있는 달 - 평달이 크고 윤달이 작으면</li>
	 * 	<li>6: 윤달이 있는 달 - 평달과 윤달이 모두 크면</li>
	 * </ul>
	 */
	public static int[][] LUNAR_DATA = {
		{2, 1, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2},
		{1, 2, 1, 1, 2, 1, 2, 5, 2, 2, 1, 2},
		{1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 1},   /* 1901 */
		{2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2},
		{1, 2, 1, 2, 3, 2, 1, 1, 2, 2, 1, 2},
		{2, 2, 1, 2, 1, 1, 2, 1, 1, 2, 2, 1},
		{2, 2, 1, 2, 2, 1, 1, 2, 1, 2, 1, 2},
		{1, 2, 2, 4, 1, 2, 1, 2, 1, 2, 1, 2},
		{1, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 1},
		{2, 1, 1, 2, 2, 1, 2, 1, 2, 2, 1, 2},
		{1, 5, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2},
		{1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 1},
		{2, 1, 2, 1, 1, 5, 1, 2, 2, 1, 2, 2},   /* 1911 */
		{2, 1, 2, 1, 1, 2, 1, 1, 2, 2, 1, 2},
		{2, 2, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2},
		{2, 2, 1, 2, 5, 1, 2, 1, 2, 1, 1, 2},
		{2, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2},
		{1, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 1},
		{2, 3, 2, 1, 2, 2, 1, 2, 2, 1, 2, 1},
		{2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2},
		{1, 2, 1, 1, 2, 1, 5, 2, 2, 1, 2, 2},
		{1, 2, 1, 1, 2, 1, 1, 2, 2, 1, 2, 2},
		{2, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2},   /* 1921 */
		{2, 1, 2, 2, 3, 2, 1, 1, 2, 1, 2, 2},
		{1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 1, 2},
		{2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 1, 1},
		{2, 1, 2, 5, 2, 1, 2, 2, 1, 2, 1, 2},
		{1, 1, 2, 1, 2, 1, 2, 2, 1, 2, 2, 1},
		{2, 1, 1, 2, 1, 2, 1, 2, 2, 1, 2, 2},
		{1, 5, 1, 2, 1, 1, 2, 2, 1, 2, 2, 2},
		{1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 2},
		{1, 2, 2, 1, 1, 5, 1, 2, 1, 2, 2, 1},
		{2, 2, 2, 1, 1, 2, 1, 1, 2, 1, 2, 1},   /* 1931 */
		{2, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2},
		{1, 2, 2, 1, 6, 1, 2, 1, 2, 1, 1, 2},
		{1, 2, 1, 2, 2, 1, 2, 2, 1, 2, 1, 2},
		{1, 1, 2, 1, 2, 1, 2, 2, 1, 2, 2, 1},
		{2, 1, 4, 1, 2, 1, 2, 1, 2, 2, 2, 1},
		{2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 2, 1},
		{2, 2, 1, 1, 2, 1, 4, 1, 2, 2, 1, 2},
		{2, 2, 1, 1, 2, 1, 1, 2, 1, 2, 1, 2},
		{2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1},
		{2, 2, 1, 2, 2, 4, 1, 1, 2, 1, 2, 1},   /* 1941 */
		{2, 1, 2, 2, 1, 2, 2, 1, 2, 1, 1, 2},
		{1, 2, 1, 2, 1, 2, 2, 1, 2, 2, 1, 2},
		{1, 1, 2, 4, 1, 2, 1, 2, 2, 1, 2, 2},
		{1, 1, 2, 1, 1, 2, 1, 2, 2, 2, 1, 2},
		{2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 1, 2},
		{2, 5, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2},
		{2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2},
		{2, 2, 1, 2, 1, 2, 3, 2, 1, 2, 1, 2},
		{2, 1, 2, 2, 1, 2, 1, 1, 2, 1, 2, 1},
		{2, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2},   /* 1951 */
		{1, 2, 1, 2, 4, 2, 1, 2, 1, 2, 1, 2},
		{1, 2, 1, 1, 2, 2, 1, 2, 2, 1, 2, 2},
		{1, 1, 2, 1, 1, 2, 1, 2, 2, 1, 2, 2},
		{2, 1, 4, 1, 1, 2, 1, 2, 1, 2, 2, 2},
		{1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2},
		{2, 1, 2, 1, 2, 1, 1, 5, 2, 1, 2, 2},
		{1, 2, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2},
		{1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1},
		{2, 1, 2, 1, 2, 5, 2, 1, 2, 1, 2, 1},
		{2, 1, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2},   /* 1961 */
		{1, 2, 1, 1, 2, 1, 2, 2, 1, 2, 2, 1},
		{2, 1, 2, 3, 2, 1, 2, 1, 2, 2, 2, 1},
		{2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2},
		{1, 2, 1, 2, 1, 1, 2, 1, 1, 2, 2, 2},
		{1, 2, 5, 2, 1, 1, 2, 1, 1, 2, 2, 1},
		{2, 2, 1, 2, 2, 1, 1, 2, 1, 2, 1, 2},
		{1, 2, 2, 1, 2, 1, 5, 2, 1, 2, 1, 2},
		{1, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 1},
		{2, 1, 1, 2, 2, 1, 2, 1, 2, 2, 1, 2},
		{1, 2, 1, 1, 5, 2, 1, 2, 2, 2, 1, 2},   /* 1971 */
		{1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 1},
		{2, 1, 2, 1, 1, 2, 1, 1, 2, 2, 2, 1},
		{2, 2, 1, 5, 1, 2, 1, 1, 2, 2, 1, 2},
		{2, 2, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2},
		{2, 2, 1, 2, 1, 2, 1, 5, 2, 1, 1, 2},
		{2, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 1},
		{2, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 1},
		{2, 1, 1, 2, 1, 6, 1, 2, 2, 1, 2, 1},
		{2, 1, 1, 2, 1, 2, 1, 2, 2, 1, 2, 2},
		{1, 2, 1, 1, 2, 1, 1, 2, 2, 1, 2, 2},   /* 1981 */
		{2, 1, 2, 3, 2, 1, 1, 2, 2, 1, 2, 2},
		{2, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2},
		{2, 1, 2, 2, 1, 1, 2, 1, 1, 5, 2, 2},
		{1, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2},
		{1, 2, 2, 1, 2, 2, 1, 2, 1, 2, 1, 1},
		{2, 1, 2, 2, 1, 5, 2, 2, 1, 2, 1, 2},
		{1, 1, 2, 1, 2, 1, 2, 2, 1, 2, 2, 1},
		{2, 1, 1, 2, 1, 2, 1, 2, 2, 1, 2, 2},
		{1, 2, 1, 1, 5, 1, 2, 1, 2, 2, 2, 2},
		{1, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 2},   /* 1991 */
		{1, 2, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2},
		{1, 2, 5, 2, 1, 2, 1, 1, 2, 1, 2, 1},
		{2, 2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2},
		{1, 2, 2, 1, 2, 2, 1, 5, 2, 1, 1, 2},
		{1, 2, 1, 2, 2, 1, 2, 1, 2, 2, 1, 2},
		{1, 1, 2, 1, 2, 1, 2, 2, 1, 2, 2, 1},
		{2, 1, 1, 2, 3, 2, 2, 1, 2, 2, 2, 1},
		{2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 2, 1},
		{2, 2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 1},
		{2, 2, 2, 3, 2, 1, 1, 2, 1, 2, 1, 2},   /* 2001 */
		{2, 2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1},
		{2, 2, 1, 2, 2, 1, 2, 1, 1, 2, 1, 2},
		{1, 5, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2},
		{1, 2, 1, 2, 1, 2, 2, 1, 2, 2, 1, 1},
		{2, 1, 2, 1, 2, 1, 5, 2, 2, 1, 2, 2},
		{1, 1, 2, 1, 1, 2, 1, 2, 2, 2, 1, 2},
		{2, 1, 1, 2, 1, 1, 2, 1, 2, 2, 1, 2},
		{2, 2, 1, 1, 5, 1, 2, 1, 2, 1, 2, 2},
		{2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2},
		{2, 1, 2, 2, 1, 2, 1, 1, 2, 1, 2, 1},   /* 2011 */
		{2, 1, 6, 2, 1, 2, 1, 1, 2, 1, 2, 1},
		{2, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2},
		{1, 2, 1, 2, 1, 2, 1, 2, 5, 2, 1, 2},
		{1, 2, 1, 1, 2, 1, 2, 2, 2, 1, 2, 1},
		{2, 1, 2, 1, 1, 2, 1, 2, 2, 1, 2, 2},
		{2, 1, 1, 2, 3, 2, 1, 2, 1, 2, 2, 2},
		{1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2},
		{2, 1, 2, 1, 2, 1, 1, 2, 1, 2, 1, 2},
		{2, 1, 2, 5, 2, 1, 1, 2, 1, 2, 1, 2},
		{1, 2, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1},   /* 2021 */
		{2, 1, 2, 1, 2, 2, 1, 2, 1, 2, 1, 2},
		{1, 5, 2, 1, 2, 1, 2, 2, 1, 2, 1, 2},
		{1, 2, 1, 1, 2, 1, 2, 2, 1, 2, 2, 1},
		{2, 1, 2, 1, 1, 5, 2, 1, 2, 2, 2, 1},
		{2, 1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2},
		{1, 2, 1, 2, 1, 1, 2, 1, 1, 2, 2, 2},
		{1, 2, 2, 1, 5, 1, 2, 1, 1, 2, 2, 1},
		{2, 2, 1, 2, 2, 1, 1, 2, 1, 1, 2, 2},
		{1, 2, 1, 2, 2, 1, 2, 1, 2, 1, 2, 1},
		{2, 1, 5, 2, 1, 2, 2, 1, 2, 1, 2, 1},   /* 2031 */
		{2, 1, 1, 2, 1, 2, 2, 1, 2, 2, 1, 2},
		{1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 5, 2},
		{1, 2, 1, 1, 2, 1, 2, 1, 2, 2, 2, 1},
		{2, 1, 2, 1, 1, 2, 1, 1, 2, 2, 1, 2},
		{2, 2, 1, 2, 1, 4, 1, 1, 2, 2, 1, 2},
		{2, 2, 1, 2, 1, 1, 2, 1, 1, 2, 1, 2},
		{2, 2, 1, 2, 1, 2, 1, 2, 1, 1, 2, 1},
		{2, 2, 1, 2, 5, 2, 1, 2, 1, 2, 1, 1},
		{2, 1, 2, 2, 1, 2, 2, 1, 2, 1, 2, 1},
		{2, 1, 1, 2, 1, 2, 2, 1, 2, 2, 1, 2},   /* 2041 */
		{1, 5, 1, 2, 1, 2, 1, 2, 2, 2, 1, 2},
		{1, 2, 1, 1, 2, 1, 1, 2, 2, 1, 2, 2}
	};

	/** 양력 월 마지막 날짜. */
	public static int[] MONTH_LAST_DAYS = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	/**
	 * Calendar.cs 에서 포팅
	 * @author dagui
	 */
	public static class CalDate implements Cloneable, Serializable {

		/** Constant serialVersionUID. */
		private static final long serialVersionUID = -282995086621977066L;

		/** year. */
		int year = 1900;

		/** month. */
		int month = 1;

		/** day. */
		int day = 1;

		/**
		 * Constructor CalDate.
		 */
		public CalDate() {
		}

		/**
		 * Constructor CalDate.
		 *
		 * @param year int
		 * @param month int
		 * @param day int
		 */
		public CalDate(int year, int month, int day) {
			set(year, month, day);
		}

		/**
		 * Constructor CalDate.
		 *
		 * @param date Date
		 */
		public CalDate(Date date) {
			set(date);
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		public boolean equals(Object o) {
			if (o instanceof CalDate) {
				CalDate c = (CalDate)o;
				return (
					year == c.year &&
					month == c.month &&
					day == c.day
				);
			}
			else {
				return false;
			}
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return year + "-" + ((month < 10)? "0": "") + month + "-" + ((day < 10)? "0": "") + day;
		}

		/**
		 * To date.
		 *
		 * @return the date
		 */
		public Date toDate() {
			Calendar cal = Calendar.getInstance();
			cal.clear();
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, month - 1);
			cal.set(Calendar.DAY_OF_MONTH, day);
			return cal.getTime();
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#clone()
		 */
		public Object clone() {
			try {
				return super.clone();
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}

		/**
		 * Sets the.
		 *
		 * @param year int
		 * @param month int
		 * @param day int
		 */
		public void set(int year, int month, int day) {
			this.year = year;
			this.month = month;
			this.day = day;
		}

		/**
		 * Sets the.
		 *
		 * @param date Date
		 */
		public void set(Date date) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			year = cal.get(Calendar.YEAR);
			month = cal.get(Calendar.MONTH + 1);
			day = cal.get(Calendar.DAY_OF_MONTH);
		}

		/**
		 * Checks if is last day of year.
		 *
		 * @return true, if is last day of year
		 */
		public boolean isLastDayOfYear() {
			return (month == 12 && day == 31);
		}

		/**
		 * Gets the last day of month.
		 *
		 * @return the last day of month
		 */
		public int getLastDayOfMonth() {
			if (month == 2)
				return (isLeapYear())? 29: 28;
			else
				return DateUtil.MONTH_LAST_DAYS[month - 1];
		}

		/**
		 * Checks if is leap year.
		 *
		 * @return true, if is leap year
		 */
		public boolean isLeapYear() {
			return DateUtil.isLeapYear(year);
		}

		/**
		 * Sets the next day.
		 */
		public void setNextDay() {
			if (isLastDayOfYear()) {
				year++;
				month = 1;
				day = 1;
			}
			else if (day == getLastDayOfMonth()) {
				month++;
				day = 1;
			}
			else {
				day++;
			}
		}

		/**
		 * Sets the prev day.
		 */
		public void setPrevDay() {
			if (month == 1 && day == 1) {
				year--;
				month = 12;
				day = 31;
			}
			else if (day == 1) {
				month--;
				day = getLastDayOfMonth();
			}
			else {
				day--;
			}
		}

		/**
		 * Gets the next day.
		 *
		 * @return the next day
		 */
		public CalDate getNextDay() {
			CalDate c = (CalDate)clone();
			c.setNextDay();
			return c;
		}

		/**
		 * Gets the prev day.
		 *
		 * @return the prev day
		 */
		public CalDate getPrevDay() {
			CalDate c = (CalDate)clone();
			c.setPrevDay();
			return c;
		}

		/**
		 * Gets the year.
		 *
		 * @return the year
		 */
		public int getYear() {
			return year;
		}

		/**
		 * Sets the year.
		 *
		 * @param year the new year
		 */
		public void setYear(int year) {
			this.year = year;
		}

		/**
		 * Gets the month.
		 *
		 * @return the month
		 */
		public int getMonth() {
			return month;
		}

		/**
		 * Sets the month.
		 *
		 * @param month the new month
		 */
		public void setMonth(int month) {
			this.month = month;
		}

		/**
		 * Gets the day.
		 *
		 * @return the day
		 */
		public int getDay() {
			return day;
		}

		/**
		 * Sets the day.
		 *
		 * @param day the new day
		 */
		public void setDay(int day) {
			this.day = day;
		}
	}

	/**
	 * <p>양력 음력 변환을 수행.</p>
	 * <p>1900 ~ 2040 년 만 변환 가능하며 범위를 벗어날 경우 입력한 날짜가 리턴됨.</p>
	 * @param calDate 계산할 날짜
	 * @param type 변환 유형 1: 양력 to 음력, 2: 음력 to 양력
	 * @return 변환된 날짜
	 */
	private static CalDate lunarCalc(CalDate calDate, int type) {
		// 1900 ~ 2040 년도까지만 변환가능. ( 지원이 안되는 날짜는 입력날짜를 그대로 리턴 )
		if (calDate.year < 1900 || calDate.year > 2040)
			return calDate;

		CalDate solDate = new CalDate();	// 양력날자
		CalDate lunDate = new CalDate();	// 음력날자

		// 계산될 초기 날짜 셋팅.
		int lunarMonthDay = initLunarCalc(calDate, solDate, lunDate);

		// 음력 윤달 여부.
		boolean isLunarLeapMonth = false;

		int lunIndex = lunDate.year - 1899;

		while (true) {
			if (type == 1) {
				if (calDate.equals(solDate))
					return lunDate;
			}
			else {
				if (calDate.equals(lunDate) && !isLunarLeapMonth)
					return solDate;
			}

			// 양력을 다음 날짜로
			solDate.setNextDay();

			if (
				lunDate.month == 12 && (
					(LUNAR_DATA[lunIndex][lunDate.month - 1] == 1 && lunDate.day == 29) ||
					(LUNAR_DATA[lunIndex][lunDate.month - 1] == 2 && lunDate.day == 30)
				)
			) {
				lunDate.year++;
				lunDate.month = 1;
				lunDate.day = 1;

				// 범위를 넘어서면
				if (lunDate.year > 2043)
					return calDate;

				lunIndex = lunDate.year - 1899;

				if (LUNAR_DATA[lunIndex][lunDate.month - 1] == 1)
					lunarMonthDay = 29;
				else if (LUNAR_DATA[lunIndex][lunDate.month - 1] == 2)
					lunarMonthDay = 30;
			}
			else if (lunDate.day == lunarMonthDay) {
				if ( LUNAR_DATA[lunIndex][lunDate.month-1] >= 3 && !isLunarLeapMonth ) {
					lunDate.day = 1;
					isLunarLeapMonth = true;
				}
				else {
					lunDate.month ++;
					lunDate.day = 1;
					isLunarLeapMonth = false;
				}

				int monthIndex = lunDate.month - 1;

				if ( LUNAR_DATA[lunIndex][monthIndex] == 1 )
					lunarMonthDay = 29;
				else if ( LUNAR_DATA[lunIndex][monthIndex] == 2 )
					lunarMonthDay = 30;
				else if ( LUNAR_DATA[lunIndex][monthIndex] == 3 )
					lunarMonthDay = 29;
				else if ( LUNAR_DATA[lunIndex][monthIndex] == 4 && !isLunarLeapMonth)
					lunarMonthDay = 29;
				else if ( LUNAR_DATA[lunIndex][monthIndex] == 4 && isLunarLeapMonth)
					lunarMonthDay = 30;
				else if ( LUNAR_DATA[lunIndex][monthIndex] == 5 && !isLunarLeapMonth)
					lunarMonthDay = 30;
				else if ( LUNAR_DATA[lunIndex][monthIndex] == 5 && isLunarLeapMonth)
					lunarMonthDay = 29;
				else if ( LUNAR_DATA[lunIndex][monthIndex] == 6 )
					lunarMonthDay = 30;
			}
			else {
				lunDate.day++;
			}
		}
	}

	/**
	 * 양력음력 변환을 위한 기준 날짜를 설정한다.
	 * @param calDate 입력 날짜
	 * @param solDate 양력 날짜 객체
	 * @param lunDate 음력 날짜 객체
	 * @return 음력 달의 날수
	 */
	private static int initLunarCalc(CalDate calDate, CalDate solDate, CalDate lunDate) {
		int lunMonthDay;
		// 기준일자 (속도개선)
		if (calDate.year >= 2000) {
			// 양력 2000년 1월 1일 (음력 1999년 11월 25일)
			solDate.set(2000, 1, 1);
			lunDate.set(1999, 11, 25);
			lunMonthDay = 30;
		}
		else if (calDate.year >= 1970) {
			// 양력 1970년 1월 1일 (음력 1969년 11월 24일)
			solDate.set(1970, 1, 1);
			lunDate.set(1969, 11, 24);
			lunMonthDay = 30;
		}
		else if (calDate.year >= 1940) {
			// 양력 1940년 1월 1일 (음력 1939년 11월 22일)
			solDate.set(1940, 1, 1);
			lunDate.set(1939, 11, 22);
			lunMonthDay = 29;
		}
		else {
			// 양력 1900년 1월 1일 (음력 1899년 12월 1일)
			solDate.set(1900, 1, 1);
			lunDate.set(1899, 12, 1);
			lunMonthDay = 30;
		}
		return lunMonthDay;
	}

	/**
	 * <p>양력 날짜를 음력 날짜로 변환한다.</p>
	 * <p>1900 ~ 2040 년 만 변환 가능하며 범위를 벗어날 경우 입력한 날짜가 리턴됨.</p>
	 * @param solarDate 양력 날짜
	 * @return 음력 날짜
	 */
	public static CalDate getLunarDate(CalDate solarDate) {
		return lunarCalc(solarDate, 1);
	}

	/**
	 * <p>음력 날짜를 양력 날짜로 변환한다.</p>
	 * <p>1900 ~ 2040 년 만 변환 가능하며 범위를 벗어날 경우 입력한 날짜가 리턴됨.</p>
	 * @param lunarDate 음력 날짜
	 * @return 양력 날짜
	 */
	public static CalDate getSolarDate(CalDate lunarDate) {
		return lunarCalc(lunarDate, 2);
	}

	/**
	 * 지정한 연도가 윤년(양력)인가?.
	 *
	 * @param year 연도
	 * @return 윤년 여부
	 */
	public static boolean isLeapYear(int year) {
		return (
			((year % 400) == 0) ||
			((year % 4) == 0 && (year % 100) != 0)
		);
	}


	/** Constant REPT_KEY_DAY. */
	public static final String REPT_KEY_DAY = "D";  // 매일

	/** Constant REPT_KEY_WEEK. */
	public static final String REPT_KEY_WEEK = "W"; // 매주

	/** Constant REPT_KEY_MONTH_DAY. */
	public static final String REPT_KEY_MONTH_DAY = "MD"; // 매월(날짜)

	/** Constant REPT_KEY_YEAR. */
	public static final String REPT_KEY_YEAR = "Y"; // 매년

	/** Constant REPT_KEY_MONTH_WEEKDAY. */
	public static final String REPT_KEY_MONTH_WEEKDAY = "MW"; //매월(요일)

	/** Constant REPT_NO_NEXT. */
	public static final String REPT_NO_NEXT = "REPT_NO_NEXT"; //다음 일자 없음

	/** Constant REPT_NO_FREQ. */
	public static final String REPT_NO_FREQ = "REPT_NO_FREQ"; //시퀀스 없음

	/**
	 * Gets the next date.
	 *
	 * @param orgDateStr String
	 * @param maxDateStr String
	 * @param reptKey String
	 * @param reptFreq int
	 * @param reptDay String
	 * @return the next date
	 */
	public static String getNextDate(String orgDateStr,String maxDateStr,String reptKey,int reptFreq,String reptDay){
		return getNextDate(orgDateStr, maxDateStr, reptKey, reptFreq, reptDay,false);
	}

	/**
	 * 최대일자가 넘지않는 선에서 원본일자의 다음 패턴에 해당하는 날짜를 가져온다.
	 * 이로직은 maxDateStr가 없을경우 값이 잘못입력되면 재수없으면 무한루프로 빠질위험이 있다.
	 * 사용시 가급적 maxDateStr을 넣을것을 당부하며 자신없으면 쓰지 말것 -_-
	 *
	 * @param orgDateStr 원본일자
	 * @param maxDateStr 최대일자
	 * @param reptKey 반복키값 REPT_KEY_DAY:매일,REPT_KEY_WEEK:매주,REPT_KEY_MONTH_DAY:매월(날짜),REPT_KEY_YEAR:매년,REPT_KEY_MONTH_WEEKDAY:매월(요일)
	 * @param reptFreq 반복주기 무조건 1이상이 들어가야함
	 * @param reptDay  0000000  : 7개의 1혹은 0의 문자열. 앞에 자리부터 일요일~ 토요일을 뜻하며 1은 해당하는 요일이 있음을 0은 없음을 뜻함
	 * @param first boolean
	 * @return the next date
	 */
	public static String getNextDate(String orgDateStr,String maxDateStr,String reptKey,int reptFreq,String reptDay,boolean first){

		if(reptFreq<=0)return REPT_NO_FREQ;
		Calendar orgCal = Calendar.getInstance();

		int year = Integer.parseInt(orgDateStr.substring(0, 4));
		int month = Integer.parseInt(orgDateStr.substring(4, 6));
		int date = Integer.parseInt(orgDateStr.substring(6, 8));
		orgCal.set(year, month - 1, date);

		if(REPT_KEY_DAY.equals(reptKey)){
			orgCal.add(Calendar.DAY_OF_MONTH,reptFreq);
			if(null != maxDateStr && !"".equals(maxDateStr)){
				String result = checkMaxDate(orgCal,maxDateStr);
				return result;
			}
			return getDate(orgCal.getTime(),"yyyyMMdd");
		}else if(REPT_KEY_WEEK.equals(reptKey)){
			if(reptDay.length() != 7 ) return REPT_NO_NEXT;
			// 1 : 일요일 ~ 7: 토요일
			// orgCal를 토요일이 될때까지 돌린후 없으면 주기만큼 주수를 더한다.
			int i;
			if(!first)orgCal.add(Calendar.DAY_OF_MONTH,1);
			for(i = orgCal.get(Calendar.DAY_OF_WEEK); i <= 7 ; i ++){
				int wday = orgCal.get(Calendar.DAY_OF_WEEK);
				if(reptDay.charAt(wday - 1) != '0'){
					return checkMaxDate(orgCal,maxDateStr);
				}
				orgCal.add(Calendar.DAY_OF_MONTH,1);
			}
			// 여기까지왔다면 해당하는 주에 해당하는 요일을 이미 지났기때문에 없는것이다.
			// 그러므로 주기만큼 한번 더 더한후 루프를 돌린다.현재 여기까지 왔다면 토요일인 상태이므로
			// 주기*7 -6 만큼 한다. 즉 해당주기의 일요일로 상태를 바꾼다.
			orgCal.add(Calendar.DAY_OF_MONTH,reptFreq * 7 - 6);
			for(i = orgCal.get(Calendar.DAY_OF_WEEK); i <= 7 ; i ++){
				int wday = orgCal.get(Calendar.DAY_OF_WEEK);
				if(reptDay.charAt(wday - 1) != '0'){
					return checkMaxDate(orgCal,maxDateStr);
				}
				orgCal.add(Calendar.DAY_OF_MONTH,1);
			}
		}else if(REPT_KEY_MONTH_DAY.equals(reptKey)){
			// 월의 구조상 어떤 반복이 들어오더라도 return 에 걸린다
			int day = orgCal.get(Calendar.DAY_OF_MONTH);
			while(true){
				orgCal.set(Calendar.DAY_OF_MONTH,1);
				orgCal.add(Calendar.MONTH,reptFreq);
				int maxDay = orgCal.getActualMaximum(Calendar.DAY_OF_MONTH);
				if(day <= maxDay){
					orgCal.set(Calendar.DAY_OF_MONTH,day);
					return checkMaxDate(orgCal,maxDateStr);
				}
			}
		}else if(REPT_KEY_YEAR.equals(reptKey)){
			// 월과 다른점은 반복패턴에 12를 곱한것뿐..
			int day = orgCal.get(Calendar.DAY_OF_MONTH);
			while(true){
				orgCal.set(Calendar.DAY_OF_MONTH,1);
				orgCal.add(Calendar.MONTH,reptFreq * 12);
				int maxDay = orgCal.getActualMaximum(Calendar.DAY_OF_MONTH);
				if(day <= maxDay){
					orgCal.set(Calendar.DAY_OF_MONTH,day);
					return checkMaxDate(orgCal,maxDateStr);
				}
			}
		}else if(REPT_KEY_MONTH_WEEKDAY.equals(reptKey)){
			// 요일
			int wDay = orgCal.get(Calendar.DAY_OF_WEEK);
			// 몇번째 주인가?
			int numWeek = (int)(orgCal.get(Calendar.DAY_OF_MONTH)/7.0 + 0.5 );
			while(true){
				orgCal.set(Calendar.DAY_OF_MONTH,1);
				orgCal.add(Calendar.MONTH,reptFreq);
				// 다음주기의 달의 1일의 요일
				int currentWday = orgCal.get(Calendar.DAY_OF_WEEK);
				currentWday = wDay - currentWday;
				if(currentWday < 0){
					currentWday = currentWday + 7;
				}
				currentWday = (numWeek - 1)* 7 + currentWday;
				int maxDay = orgCal.getActualMaximum(Calendar.DAY_OF_MONTH);
				if(currentWday <= maxDay){
					orgCal.set(Calendar.DAY_OF_MONTH,currentWday);
					return checkMaxDate(orgCal,maxDateStr);
				}
			}
		}

		return REPT_NO_NEXT;

	}

	/**
	 * Check max date.
	 * @param orgCal Calendar
	 * @param maxDateStr String
	 * @return the string
	 */
	private static String checkMaxDate(Calendar orgCal,String maxDateStr){
		if(null != maxDateStr && !"".equals(maxDateStr)){
			Calendar maxCal = Calendar.getInstance();

			int maxYear = Integer.parseInt(maxDateStr.substring(0, 4));
			int maxMonth = Integer.parseInt(maxDateStr.substring(4, 6));
			int maxDate = Integer.parseInt(maxDateStr.substring(6, 8));
			maxCal.set(maxYear, maxMonth - 1, maxDate);
			if(orgCal.compareTo(maxCal) > 0 ){
				return REPT_NO_NEXT;
			}
		}
		return getDate(orgCal.getTime(),"yyyyMMdd");
	}

	
	/**
	 * Calendar 객체와 format 을 받아서 문자열로 반환
	 * @param cal 날짜 또는 시간을 가지고 있는 객체
	 * @param format	반환할 format 형식 [값이 없는경우 기본형식으로 반환함 "yyyy-MM-dd HH:mm:ss"]
	 */
	public static String getCalendarFormat(Calendar cal, String format){
		String defaultFormat = "yyyy-MM-dd HH:mm:ss";
		
		//전달된 format 이 없다면
		if (format != null && format.length() > 0){
			defaultFormat = format;
		}
		
		SimpleDateFormat formatter = new SimpleDateFormat(defaultFormat);         
		return formatter.format(cal.getTime());
	}
	
	/**
	 * Calendar 객체와 format 을 받아서 문자열로 반환
	 * @param yyyyMMdd "yyyyMMdd" 형식의 날짜 문자열
	 * @param format		반환할 format 형식 [값이 없는경우 기본형식으로 반환함 "yyyy-MM-dd HH:mm:ss"]
	 * @return <p>yyyyMMdd를 주어진 format형식으로 반환함
	 * 단, yyyyMMdd 없거나 형식에 맞지 않으면 ""를 반환함</p>
	 */
	public static String getStrDateToCalendarFormat(String yyyyMMdd, String format){
		Calendar cal = Calendar.getInstance();
		
		//전달된 날짜문자열이 존재하면서 형식에 틀렸다면 empty를 반환함
		if(yyyyMMdd == null || yyyyMMdd.length() != 8){
			return "";
		}
		
		Date date = DateUtil.StringtoDate(yyyyMMdd);
		cal.setTime(date);
		
		return getCalendarFormat(cal, format);
		//Calendar
	}
	
	/**
	 * <p>문자열 시간을 받아서[hhmm] Calendar 객체에 시간을 셋팅
	 * 비어 있지 않으면서 3자리이상 숫자였을 경우만 처리함 
	 * @param hhmm		hhmm 형식의 시간 문자열
	 * @return			전달받은 시간으로 셋팅해서 Calendar 객체 반환
	 * 단, 전달받은 시간이  없다면 00시 00분으로 셋팅하여 반환
	 * </p>
	 */
	public static Calendar getStringTimeToCalendar(String hhmm){
		Calendar cal = Calendar.getInstance();
		
		//00시 00분으로 셋팅
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		
		try{
			//비어 있지 않으면서 4자리 숫자였을 경우만 처리함
			if(hhmm != null && !"".equals(hhmm) && hhmm.length() > 2 && Integer.parseInt(hhmm) > 0){
				//요청된 시간으로 셋팅
				cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hhmm.substring(0, hhmm.length()-2)));
				cal.set(Calendar.MINUTE, Integer.parseInt(hhmm.substring(hhmm.length()-2)));
				cal.set(Calendar.SECOND, 0);
			}
		}catch(Exception e){
			System.out.println("error To DateUti.getStringTimeToCalendar()");
			e.printStackTrace();
		}
		
		return cal;
	}
	
	/**
	 * <p>문자열 시간을 받아서[hhmm] minutes 분만큼 시간을 추가하여 반환
	 * @param hhmm		hhmm 형식의 시간 문자열
	 * @param minutes	추가될 분단위 시간[+/- 모두 올수 있음]
	 * @return	시간을 추가된 값으로 셋팅된 Calendar 객체 반환
	 * </p>
	 */
	public static Calendar addStrTimeAndIntMinuteToCalendar(String hhmm, int minutes){
		Calendar cal = getStringTimeToCalendar(hhmm);
//		System.out.println("before : " + getCalendarFormat(cal, "yyy-MM-dd HH:mm:ss"));
		cal.add(Calendar.MINUTE, minutes);
//		System.out.println("after : " + getCalendarFormat(cal, "yyy-MM-dd HH:mm:ss"));
		
		return cal;
	} 

	/**
	 * 시스템 Today와 전달된 날짜차이를 반환함
	 * 
	 * @param yyyymmdd 비교할 문자열 날짜
	 * @return 과거 < 0 [차이나는 날짜만큼 음수값], 현재 == 0, 미래 > 0 [차이나는 날짜만큼 양수값]
	 */
	public static int getDateDifferenceStringDateAndSystemToday(String yyyymmdd){
		//과거 < 0 [차이나는 날짜만큼 음수값], 현재 == 0, 미래 > 0 [차이나는 날짜만큼 양수값]
		String dateDifference = DateUtil.dateDifference(DateUtil.getTodayStart(), DateUtil.StringtoDate(yyyymmdd));
		int result = Integer.parseInt(dateDifference);
		
		return result;
	}
	

    public static Date toDate(String pubDate){
		String[] formatString = {
				"E, d MMM yyyy hh:mm:ss z", 
				"E, d MMM yyyy HH:mm:ss Z", 
				"E, dd MMM yyyy HH:mm:ss Z",
				"E, dd MMM yyyy hh:mm:ss z", 
				"E, dd MMM yyyy hh:mm:ss", 
				"E, d MMM yyyy hh:mm:ss",
				"yyyy+MM+dd H:mm:ss", 
				"yyyy년 MM월 dd일 HH:mm:ss", 
				"yyyy+MM+dd'T'H:mm:ss",
				"yyyy+MM+dd'T'H:mm:ssZ", 
				"yyyy+MM+dd h:mm:ss a", 
				"dd MMM yyyy HH:mm:ss",
				"d MMM yyyy HH:mm:ss", 
				"yyyy/MM/dd", 
				"yyyy-MM-dd",
				"yyyyMMddHHmmss", 
				"yyyyMMdd" 
				};
		
		ArrayList dateFormatList = new ArrayList();
		for(int i = 0 ;i < formatString.length ;i++){
			String dateFormat = formatString[i];
			dateFormatList.add(new SimpleDateFormat(dateFormat, Locale.US));
		}
		
		Date date = null;
		pubDate = pubDate.replaceAll("GMT", "").trim();
			for(int i=0;i<dateFormatList.size() ;i++){
					try {
						 date = ((SimpleDateFormat)dateFormatList.get(i)).parse(pubDate);
						 if(date!=null)
							 i=dateFormatList.size();
					} catch (ParseException e) { ;}//l.log(e.toString()); }
			}
			if(date==null){
				//date = new Date();
				date = null;
			}
		return date;
	}

	public static String getYesterday(String format) {
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat(format);
		cal.add(Calendar.DATE, -1);

		return dateFormat.format(cal.getTime());
	}
	
	/**
	 * 주어진 날짜를 '디폴트로케일' 패턴에 맞는 날짜 문자열을 반환한다.
	 * @param date Date 객체
	 * @param pattern 구하고자 하는 날짜 패턴
	 * @return 입력받은 Date 객체로부터 입력받은 패턴에 맞게 생성된 날짜 문자열을 반환한다. 만일 Date 객체가 null
	 *         이거나 pattern이 null 이라면 공백 문자열("")을 반환한다.
	 */
	public static String getDateByDefaultLocale(Date date, String pattern) {
		if (date == null || pattern == null) {
			return "";
		}
		SimpleDateFormat sf = new SimpleDateFormat(pattern);
		return sf.format(date);
	}
	
	public static String prevMonth(String year, String month){
		String yearMonth = "";
		if(month.equals("01")){
			yearMonth = String.valueOf(NumberUtils.toInt(year) - 1) + "12";
		}else{
			yearMonth = year + checkZero(NumberUtils.toInt(month) - 1);
		}
		return yearMonth;
	}
	
	public static String nextMonth(String year, String month){
		String yearMonth = "";
		if(month.equals("12")){
			yearMonth = String.valueOf(NumberUtils.toInt(year) + 1) + "01";
		}else{
			yearMonth = year + checkZero(NumberUtils.toInt(month) + 1);
		}
		return yearMonth;
	}
	
	private static String checkZero(int month){
		if(month < 10) return "0" + String.valueOf(month);
		else return String.valueOf(month);
	}
	
	/**
	 * 초를 입력받아 일 시 분 초 로 표현
	 */
	public static String getTimeStr(int second){
		String result = "";
		int d; //일
		int h; //시
		int m; //분
		int s; //나머지초
		
		d = second/86400; // 1일을 초로환산한 86400초를 나눔
		h = (second - (d * 86400)) / 60 / 60; //(입력-일수50일:나머지)/분/초
		m = ((second - (d * 86400)) - h * 60 * 60) / 60;
		s = second % 60;
		
		if(d > 0){
			result += (d + "일");
		}
		
		if(h > 0){
			result += (h + "시");
		}
		
		if(m > 0){
			result += (m + "분");
		}
		
		if(s > 0){
			result += (s + "초");
		}
		return result;
	}
	
	/**
	 * 오늘 요일을 구한다.
	 */
	public static String getCurrentDayOfWeek(){
		Calendar calendar = Calendar.getInstance();
		int iDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK); //요일을 구한다
		
		String strDayOfWeek = "";
		
		switch(iDayOfWeek){
			case 1:
				strDayOfWeek = "일요일";
		         break;
			case 2: 
				strDayOfWeek = "월요일";
				break;
			case 3: 
				strDayOfWeek = "화요일";
				break;
			case 4: 
				strDayOfWeek = "수요일";
				break;
			case 5: 
				strDayOfWeek = "목요일";
				break;
			case 6: 
				strDayOfWeek = "금요일";
				break;
			case 7: 
				strDayOfWeek = "토요일";
				break;
		}
		return strDayOfWeek;
	}
	
	/**
	 * 초단위 계산
	 */
	public static String addSec(long sec){
		return addSec(sec, null);
	}
	
	/**
	 * 초단위 계산
	 */
	public static String addSec(long sec, String pattern){
		if(pattern == "" || pattern == null) pattern = "yyyyMMddHHmmss";
		return getDate(getCurrentTimeMillis() + (sec * 1000), pattern);
	}
	
	public static String convertTimeLongToString(Long time){
		String str = "";
		if(time == null || time == 0) return str;
		
		str = String.valueOf(time);
		if(time.toString().length() == 12){
			return str.substring(0,4) + "-" + str.substring(4,6) + "-" + str.substring(6,8) + " " + str.substring(8,10) + ":" + str.substring(10,12);
		}else{
			return str.substring(0,4) + "-" + str.substring(4,6) + "-" + str.substring(6,8) + " " + str.substring(8,10) + ":" + str.substring(10,12) + ":" + str.substring(12,14);
		}
	}
	
	public static String getConvertSecondTime(int seconds) {

	    int hours = seconds / 3600;
	    int minutes = (seconds % 3600) / 60;
	    seconds = seconds % 60;

	    return twoDigitString(hours) + ":" + twoDigitString(minutes) + ":" + twoDigitString(seconds);
	}

	public static String twoDigitString(int number) {

	    if (number == 0) {
	        return "00";
	    }

	    if (number / 10 == 0) {
	        return "0" + number;
	    }

	    return String.valueOf(number);
	}
	
	/**
	 * Data 날짜를 원하는 String 형식으로 변경
	 * @param date : 날짜형
	 * @param format : 형식 ex)'yyyy-mm-dd'
	 * @return String : 형식의 결과값값
	 */
	public static String getFormatDate(java.util.Date date, String format) {
		if (date == null || format == null)
			return "";

		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}
	
	public static String getFormatDate(String date) {
		if (date == null)
			return "";
		
		Date p_date = null;
		
		try {
			p_date = formatter.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}

		SimpleDateFormat sdf = new SimpleDateFormat(DATEFORMAT);
		return sdf.format(p_date);
	}
	
	public static long getFormatDateToLong(String date) {
		if (date == null)
			return 0;
		
		Date p_date = null;
		
		try {
			p_date = formatter.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return p_date.getTime();
	}
	
	public static String getFormatDate(String date, String format) {
		if (date == null || format == null)
			return "";
		
		Date p_date = null;
		
		try {
			p_date = formatter.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}

		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(p_date);
	}
	
	public static String getFormatDate(String date, String inPutFormat, String outPutFormat) {
		if (date == null || inPutFormat == null || outPutFormat == null)
			return date;
		
		Date p_date = null;
		
		try {
			
			SimpleDateFormat formatter = new SimpleDateFormat(inPutFormat);
			p_date = formatter.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
			return date;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(outPutFormat);
		return sdf.format(p_date);
	}
	
	public static Date getFormatDateAtDate(String date, String inPutFormat) {
		if (date == null || inPutFormat == null)
			return null;
		
		Date p_date = null;
		
		try {
			
			SimpleDateFormat formatter = new SimpleDateFormat(inPutFormat);
			p_date = formatter.parse(date);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return p_date;
	}
	
	public static String getMilToFormatDate(long mil, String format){
		return getFormatDate(new Date(mil), format);
	}
	
	/**
	 * 현재시간리턴
	 * @return
	 */
	public static int getCurrentHour() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		return cal.get(Calendar.HOUR_OF_DAY);
	}
	
	/**
	 * 주어진시간범위안에 현재시간이 포함되는지 여부
	 * @param start
	 * @param end
	 * @return
	 */
	public static boolean isContains24Hour(int start, int end){
		return getHourArray(start, end).contains(getCurrentHour());
	}
	
	/**
	 * 시작/종료에 비례한 24시간 배열리턴
	 * @param start
	 * @param end
	 * @return
	 */
	public static List<Integer> getHourArray(int start, int end){
		
		List<Integer> result = new ArrayList<Integer>();
		
		for (int i = start; i < 24; i++) {
			result.add(i);
		}
		
		for (int i = 0; i < end; i++) {
			result.add(i);
		}
		
		return result;
	}
}
