package com.workerman.server.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class FileUtil {

	/**
	 * 저장할 파일경로 폴더 생성
	 */
	public static String makeDirectory(String rootPath, String id, String ranNum, String alpaNum) {
		StringBuilder makeDirStr = new StringBuilder();
		long timeMillis = Calendar.getInstance().getTimeInMillis();
		makeDirStr.append(rootPath);
		makeDirStr.append("/");
		makeDirStr.append(DateUtil.getFormatDate(new Date(), "yyyyMMdd"));
		makeDirStr.append("/");
		makeDirStr.append(StringUtil.randomNumber(Integer.parseInt(ranNum)));
		makeDirStr.append("/");
		makeDirStr.append(id);
		makeDirStr.append("_");
		makeDirStr.append(timeMillis);
		makeDirStr.append(StringUtil.alphabetNumberMixing(Integer.parseInt(alpaNum)));
		makeDirStr.append("/");
		File newDir = new File(makeDirStr.toString());
		if(!newDir.exists()) {
			boolean isMakes = newDir.mkdirs();
			if(isMakes) {
				return makeDirStr.toString();
			}else {
				return null;
			}
		}else {
			return makeDirStr.toString();
		}
	}
	
	/**
     * 로그 폴더 생성
     */
	public static String makeLogDirectory(String rootPath, String flag) {
        StringBuilder makeDirStr = new StringBuilder();
        makeDirStr.append(rootPath);
        //makeDirStr.append("/"); // properties 경로 끝에 /가 이미 지정되어있음
        makeDirStr.append(DateUtil.getFormatDate(new Date(), "yyyy"));
        makeDirStr.append("/");
        makeDirStr.append(DateUtil.getFormatDate(new Date(), "MM"));
        makeDirStr.append("/");
        if(flag == "S") {
            makeDirStr.append("site");
            makeDirStr.append("/");
        } else if(flag == "V") {
            makeDirStr.append("video");
            makeDirStr.append("/");
        }
        
        File newDir = new File(makeDirStr.toString());
        if(!newDir.exists()) {
            boolean isMakes = newDir.mkdirs();
            if(isMakes) {
                return makeDirStr.toString();
            }else {
                return null;
            }
        }else {
            return makeDirStr.toString();
        }
    }
	
	/**
	 * 최상위경로 + 현재날짜 + 아이디 
	 * 조합 폴더 생성
	 */
	public static String makeDirectory(String rootPath, String id, String datePattern) {
		StringBuilder makeDirStr = new StringBuilder();
		makeDirStr.append(rootPath);
		makeDirStr.append("/");
		makeDirStr.append(DateUtil.getFormatDate(new Date(), "yyyyMMdd"));
		makeDirStr.append("/");
		makeDirStr.append(id);
		makeDirStr.append("/");
		File newDir = new File(makeDirStr.toString());
		if(!newDir.exists()) {
			boolean isMakes = newDir.mkdirs();
			if(isMakes) {
				return makeDirStr.toString();
			}else {
				return null;
			}
		}else {
			return makeDirStr.toString();
		}
	}
	
	/**
	 * 최상위경로  + 아이디 
	 * 조합 폴더 생성
	 */
	public static String makeDirectory(String rootPath, String id) {
		StringBuilder makeDirStr = new StringBuilder();
		makeDirStr.append(rootPath);
		makeDirStr.append("/");
		makeDirStr.append(id);
		makeDirStr.append("/");
		File newDir = new File(makeDirStr.toString());
		if(!newDir.exists()) {
			boolean isMakes = newDir.mkdirs();
			if(isMakes) {
				return makeDirStr.toString();
			}else {
				return null;
			}
		}else {
			return makeDirStr.toString();
		}
	}
	
	/**
     * 사용자 다운로드 폴더생성
     */
    public static String makeFileDirectory(String rootPath, String user_id) {
        StringBuilder makeDirStr = new StringBuilder();
        makeDirStr.append(rootPath);
        makeDirStr.append(DateUtil.getFormatDate(new Date(), "yyyy"));
        makeDirStr.append("/");
        makeDirStr.append(DateUtil.getFormatDate(new Date(), "MM"));
        makeDirStr.append("/");
        makeDirStr.append(DateUtil.getFormatDate(new Date(), "dd"));
        makeDirStr.append("/");
        makeDirStr.append("UserPage");
        makeDirStr.append("/");
        makeDirStr.append(user_id);
        makeDirStr.append("/");
        
        File newDir = new File(makeDirStr.toString());
        if(!newDir.exists()) {
            boolean isMakes = newDir.mkdirs();
            if(isMakes) {
                return makeDirStr.toString();
            }else {
                return null;
            }
        }else {
            return makeDirStr.toString();
        }
    }
	
	/**
	 * 파일 삭제
	 */
	public static boolean removeFile(String filePath) {
		File file = new File(filePath);
		if(file.exists()) {
			file.delete();
			return true;
		}
		return false;
	}
	
	/**
	 *	파일  디렉토리 및 파일 삭제
	 */
	public static void deleteSubFile(File file) {
		if (file.isDirectory()) {
			if (file.listFiles().length != 0) {
				File[] fileList = file.listFiles();
				for (int i = 0; i < fileList.length; i++) {

					// 디렉토리이고 서브 디렉토리가 있을 경우 리커젼을 한다...
					deleteSubFile(fileList[i]);
					file.delete();
				}
			} else {

				// 파일 트리의 리프까지 도달했을때 삭제...
				file.delete();
			}
		} else {

			// 파일 일 경우 리커젼 없이 삭제...
			file.delete();
		}
	}
	
	/**
	 * 서브파일 삭제
	 */
	public static void deleteSubFile(String path) {
		File file = new File(path);
		if (file.isDirectory()) {
			if (file.listFiles().length != 0) {
				File[] fileList = file.listFiles();
				for (int i = 0; i < fileList.length; i++) {

					// 디렉토리이고 서브 디렉토리가 있을 경우 리커젼을 한다...
					deleteSubFile(fileList[i]);
					file.delete();
				}
			} else {

				// 파일 트리의 리프까지 도달했을때 삭제...
				file.delete();
			}
		} else {

			// 파일 일 경우 리커젼 없이 삭제...
			file.delete();
		}
	}
	
	public static String saveFile(MultipartFile file, String basePath, String fileName) throws IOException {
		
		String serverFullPath = basePath + fileName;
		
		InputStream fis = new BufferedInputStream(file.getInputStream());
		FileOutputStream fos = new FileOutputStream(serverFullPath);
		
		int byteRead = 0;
		byte[] buffer = new byte[1024];
		while((byteRead = fis.read(buffer, 0, 1024)) != -1) {
			fos.write(buffer, 0, byteRead);
		}
		
		fos.close();
		fis.close();
		return serverFullPath;
		
	}
	
}
