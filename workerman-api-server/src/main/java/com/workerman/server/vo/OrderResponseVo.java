package com.workerman.server.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class OrderResponseVo extends ResultVo{
	
	@ApiModelProperty(value = "주문관리번호") private int o_idx;
	@ApiModelProperty(value = "회원관리번호") private int u_idx;
	@ApiModelProperty(value = "주문가격") private int price;
	@ApiModelProperty(value = "주문상세이미지") private String content_img;
	@ApiModelProperty(value = "주문상세내용") private String content_txt;
	@ApiModelProperty(value = "주문제목") private String title;
	@ApiModelProperty(value = "상태명") private String state_name;
	@ApiModelProperty(value = "사용자ID") private String user_id;
	@ApiModelProperty(value = "갱신일자") private String upd_dt;
	@ApiModelProperty(value = "등록일자") private String reg_dt;
	
}
