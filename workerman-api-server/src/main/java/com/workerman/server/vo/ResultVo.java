package com.workerman.server.vo;

import java.io.Serializable;

import com.workerman.server.common.ErrorCode;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ResultVo implements Serializable {

    private static final long serialVersionUID = -6508033093145249590L;

    @ApiModelProperty(value = "응답 코드")
	private String returnCd = ErrorCode.COMPLETE_PROCESS.getCode();

    @ApiModelProperty(value = "응답 메세지")
    private String returnMsg = ErrorCode.COMPLETE_PROCESS.getMessage();
    
}