package com.workerman.server.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 페이징 Value Of Object
 */
@Data
public class PageMakerVo extends PageVo implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final int DEFAULT_PAGING = 1;
	public static final int DEFAULT_PAGING_GROUP = 10;
	public static final int PAGESIZE_4 = 4;
	public static final int PAGESIZE_5 = 5;
	public static final int PAGESIZE_10 = 10;
	public static final int PAGESIZE_15 = 15;
	public static final int PAGESIZE_20 = 20;

	@ApiModelProperty(value = "게시판 구분별 총 게시글 수 ") private int total_record; /* 게시판 구분별 총 게시글 수 */
	@ApiModelProperty(hidden = true) private int total_page; /* 페이징 전체페이지수 */
	@ApiModelProperty(hidden = true) private int page_group = DEFAULT_PAGING_GROUP; /* 페이징 그룹 수 */

	/**
	 * 페이징 처리 (첫 페이지) 값 가져오기
	 */
	public int getPage_first() {

		if (now_page > 1)
			page_first = page_size * (now_page - 1);
		else
			page_first = 0;

		return page_first;
	}

	/**
	 * 페이징 처리 (첫 페이지) 값 설정
	 */
	public void setPage_first(int pageFirst) {
		this.page_first = pageFirst;
	}

	/**
	 * 페이징 처리 (현재 페이지) 값 가져오기
	 */
	public int getNow_page() {
		return now_page > 0 ? now_page : 1;
	}

	/**
	 * 페이징 처리 (현재 페이지) 값 설정
	 */
	public void setNow_page(int nowPage) {
		this.now_page = nowPage;
	}

	/**
	 * 페이징 처리 (한 페이지에 나타나는 게시글 수 10) 값 가져오기
	 */
	public int getPage_size() {
		return page_size;
	}

	/**
	 * 페이징 처리 (한 페이지에 나타나는 게시글 수 10) 값 설정
	 */
	public void setPage_size(int pageSize) {
		this.page_size = pageSize;
	}

	/**
	 * 페이징 전체페이지수 값 가져오기
	 */
	public int getTotal_page() {
		return total_page > 0 ? total_page : 1;
	}

	/**
	 * 페이징 전체페이지수 값 설정
	 */
	public void setTotal_page(int totalPage) {
		this.total_page = totalPage;
	}

	/**
	 * 페이징 그룹 수 값 가져오기
	 */
	public int getPage_group() {
		return page_group;
	}

	/**
	 * 페이징 그룹 수 값 설정
	 */
	public void setPage_group(int pageGroup) {
		this.page_group = pageGroup;
	}

	/**
	 * 게시판 구분별 총 게시글 수 값 가져오기
	 */
	public int getTotal_record() {
		return total_record;
	}

	/**
	 * 게시판 구분별 총 게시글 수 값 설정
	 */
	public void setTotal_record(int totalRecord) {
		this.total_record = totalRecord;
		setTotal_page(getTotal_page(totalRecord, getPage_size()));
	}

	/**
	 * 페이징용 - 전체페이지 개수 구하기
	 * 
	 * @param totalRecord
	 *            : 전체 레코드수
	 * @param pageSize
	 *            : 페이지 사이즈
	 * @return : 전체페이지 수
	 */
	public static int getTotal_page(int totalRecord, int pageSize) {
		int totalPage = 0;
		if (totalRecord > 0) {
			totalPage = totalRecord / pageSize;
			if (totalRecord % pageSize != 0) {
				totalPage = totalPage + 1;
			}
		}

		// 레코드가 없으면 현재페이지와 전체페이지는 1
		if (totalPage == 0) {
			totalPage = 1;
		}

		return totalPage;
	}

}
