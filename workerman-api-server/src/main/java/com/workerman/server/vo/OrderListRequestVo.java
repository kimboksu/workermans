package com.workerman.server.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class OrderListRequestVo extends PageMakerVo{
	
	/**
	 * 조회조건
	 */
	@ApiModelProperty(value = "주문제목", required = false, allowEmptyValue = true) private String title;
	@ApiModelProperty(value = "사용자ID", required = false, allowEmptyValue = true) private String user_id;
	
}
