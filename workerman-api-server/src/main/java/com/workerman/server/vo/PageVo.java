package com.workerman.server.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 페이징 Value Of Object
 */
@Data
public class PageVo{

	@ApiModelProperty(value = "한 페이지에 나타나는 게시글 수", required =  true) protected int page_size; /* 페이징 처리 (한 페이지에 나타나는 게시글 수. 10) */
	@ApiModelProperty(value = "현재 페이지", required =  true) protected int now_page; /* 페이징 처리 (현재 페이지) */
	@ApiModelProperty(hidden = true) protected int page_first; /* 페이징 처리 (첫 페이지) */

}
