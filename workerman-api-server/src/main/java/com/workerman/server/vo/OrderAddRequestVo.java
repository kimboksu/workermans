package com.workerman.server.vo;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class OrderAddRequestVo{
	
	@ApiModelProperty(value = "주문가격", required = true, allowEmptyValue = false) @NotNull @Min(value = 1) private int price;
	@ApiModelProperty(value = "상태번호", required = true, allowEmptyValue = false, hidden = true) private int state_idx;
	@ApiModelProperty(value = "회원관리번호", required = true, allowEmptyValue = false) @NotNull @Min(value = 1) private int u_idx;
	@ApiModelProperty(value = "주문상세글", required = true, allowEmptyValue = false) @NotNull private String content_txt;
	@ApiModelProperty(value = "주문제목", required = true, allowEmptyValue = false) @NotNull private String title;
	@ApiModelProperty(value = "주문상세이미지", hidden = true) private String content_img;
	
}
