package com.workerman.server.vo;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class OrderListResponseResultVo extends PageResponseVo{
	
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "주문목록") private List<OrderListResponseVo> list;
	
}
