package com.workerman.server.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 페이징 Value Of Object
 */
@Data
public class PageResponseVo extends ResultVo implements Serializable {

	@ApiModelProperty(value = "한 페이지에 나타나는 게시글 수") protected int page_size; /* 페이징 처리 (한 페이지에 나타나는 게시글 수. 10) */
	@ApiModelProperty(value = "현재 페이지") protected int now_page; /* 페이징 처리 (현재 페이지) */
	@ApiModelProperty(value = "게시판 구분별 총 게시글 수 ") private int total_record; /* 게시판 구분별 총 게시글 수 */

}
