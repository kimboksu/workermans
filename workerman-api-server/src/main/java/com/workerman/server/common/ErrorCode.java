package com.workerman.server.common;

public enum ErrorCode {

	COMPLETE_PROCESS("0000", "처리가 완료되었습니다."),
	FAIL_SYSTEM_ERROR("0100", "시스템 오류가 있습니다."),
	FAIL_INVALID_INPUT("0200", "입력값에 오류가 있습니다."),
	FAIL_AUTH("0300", "인증이 실패되었습니다. 처리가 중단됩니다."),
	EMPTY_DATA("0400", "데이터가 존재하지않습니다."),
	;

	private String code;
	private String message;

	ErrorCode(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

	public static String getMessage(String code) {
		for (ErrorCode errorCode : ErrorCode.values()) {
			if (errorCode.getCode().equals(code)) {
				return errorCode.getMessage();
			}
		}

		return "";
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
